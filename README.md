# ** # NET.Core 3.1 / Angular9 / Fomantic UI / Covalent UI / ElectronJS ** #

1. NET.Core SDK
2. VSCODE


### Screenshots
![Login](/images/2020-07-27_20_49_40-TokApp.png)
![Dashboard](/images/2020-07-27_20_50_28-TokApp.png)
![Dashboard - Widgets](/images/2020-07-27_20_50_55-TokApp.png)
![Dashboard - Widgets](/images/2020-07-27_20_51_13-TokApp.png)
![Dashboard - Styles](/images/2020-07-27_20_52_31-TokApp.png)
![Users](/images/2020-07-27_20_51_21-TokApp.png)
![Users - Create](/images/2020-07-27_20_51_27-TokApp.png)
![Monitor](/images/2020-07-27_20_51_37-TokApp.png)
![Profile](/images/2020-07-27_20_51_44-TokApp.png)
![Lock User](/images/2020-07-27_20_51_52-TokApp.png)

### Components

* https://material.angular.io/components
* https://teradata.github.io/covalent/
* https://material.angular.io/components/
* https://materialdesignicons.com/
* https://www.materialui.co/colors/
* https://fomantic-ui.com/
