import { Component, OnInit, AfterViewInit, HostListener, ElementRef, ViewChild, HostBinding } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService, DefaultLangChangeEvent } from '@ngx-translate/core';

import { AppConfig } from './app.config';
import { authConfig } from './auth.config';

import { UserModel } from './shared/models/user.model';

import { NgxMAuthOidcService } from './core/services/oidc/oidc.service';
import { AccountService } from './core/services/account.service';
import { SidebarService } from './core/services/sidebar.service';

import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { OverlayContainer } from '@angular/cdk/overlay';
import { map } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';

declare var $: any;

const THEME_DARKNESS_SUFFIX = `-dark`

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {

  userLogged: UserModel;
  languages: any[] = AppConfig.settings.i18n.availableLanguages;

  email;

  @ViewChild('header', { read: ElementRef }) headerRef: ElementRef;
  @ViewChild('menu', { read: ElementRef }) menuRef: ElementRef;
  @HostBinding('class') activeThemeCssClass: string;

  // For navigation (sidenav/toolbar). Isn't related to dynamic-theme-switching:
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(result => result.matches)
  )

  constructor(
    private breakpointObserver: BreakpointObserver,
    private overlayContainer: OverlayContainer,
    private route: ActivatedRoute,
    private router: Router,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private oidcService: NgxMAuthOidcService,
    public sidebarService: SidebarService,
    public accountService: AccountService) { }

  @HostListener('window:scroll', ['$event'])
  scrollHandler(event): void {
    const windowTop = event.target.scrollTop;
    if (windowTop >= 50) {
      if (this.menuRef) {
        this.menuRef.nativeElement.classList.remove('hidden');
        this.menuRef.nativeElement.classList.add('fade-in');
      }
    } else {
      if (this.menuRef) {
        this.menuRef.nativeElement.classList.add('hidden');
        this.menuRef.nativeElement.classList.remove('fade-in');
      }
    }

    this.sidebarService.subheaderVisible = (windowTop >= 50);
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);

    this.sidebarService.theme.subscribe(value => this.setActiveTheme(value, this.sidebarService.isThemeDark));

    const theme = JSON.parse(localStorage.getItem('style'));
    const dark = JSON.parse(localStorage.getItem('dark'));
    this.setActiveTheme((theme == null ? 'style-murek': theme), dark);

    localStorage.setItem('style', JSON.stringify(this.sidebarService.activeTheme));

    this.oidcService.configure(authConfig(AppConfig.settings.oidc.apiUrl));
    this.oidcService.setStorage(sessionStorage);
    this.oidcService.loadDocumentAndTryLogin({ customHashFragment: '?' })
    .then(res => {
      if (this.accountService.getLoggedIn()) {
        this.accountService.getUserLogged();
      }
    })
    .catch(err => {
      console.error('loadDocumentAndTryLogin error', err);
    });

    this.oidcService.events.subscribe(e => {
      switch (e.type) {
        case 'document_loaded': {
          break;
        }
        case 'token_received': {
          this.accountService.getUserLogged();
          break;
        }
        case 'token_expires': {
          this.oidcService.scope = AppConfig.settings.oidc.scope;
          this.oidcService.refreshToken()
              .then()
              .catch(err => {
                console.error(e, err);
              });
          break;
        }
        case 'token_error':
        case 'token_refresh_error': {
          this.accountService.logoutUser();
          this.router.navigate(['/login']);
          break;
        }

        case 'logout': {
          this.router.navigate(['/login']);
          break;
        }
      }
    });
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  private initSemanticConfig() {
  }

  public send() {
  }

  private setActiveTheme(theme: string, darkness: boolean = null) {
    this.tdLoadingService.register();

    if (darkness === null) {
      darkness = this.sidebarService.isThemeDark;
    } else if (this.sidebarService.isThemeDark === darkness) {
      if (this.sidebarService.activeTheme === theme) {

      }
    } else {
      this.sidebarService.isThemeDark = darkness;
    }

    this.sidebarService.activeTheme = theme;
    const cssClass = darkness === true ? theme + THEME_DARKNESS_SUFFIX : theme;
    const classList = this.overlayContainer.getContainerElement().classList;
    if (classList.contains(this.activeThemeCssClass)) {
      classList.replace(this.activeThemeCssClass, cssClass);
    } else {
      classList.add(cssClass);
    }
    this.activeThemeCssClass = cssClass;

    setTimeout(_ => {
      this.tdLoadingService.resolve();
    }, 500);
  }
}
