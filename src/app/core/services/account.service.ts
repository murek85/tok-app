import { Injectable } from '@angular/core';
import { Router, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, forkJoin, Observable } from 'rxjs';

import { UserModel } from 'src/app/shared/models/user.model';

import { NgxMAuthOidcService } from './oidc/oidc.service';

import { UsersHttpService } from './http/users.service';
import { PermissionsTypes } from 'src/app/shared/enums/permissions-types.enum';
import { NgxPermissionsService } from 'ngx-permissions';

@Injectable({ providedIn: 'root' })
export class AccountService {

  permissions = [
    PermissionsTypes.DASHBOARD,
    PermissionsTypes.FINANCES,
    PermissionsTypes.HR,
    PermissionsTypes.WORKTIMES,
    PermissionsTypes.USERS,
    PermissionsTypes.USER_CREATE,
    PermissionsTypes.USER_EDIT,
    PermissionsTypes.GROUPS,
    PermissionsTypes.PERMISSIONS,
    PermissionsTypes.MONITOR,
  ];

  loginSocialProviders: any[] = [
    { name: 'Google', key: 'Google', icon: 'google', class: 'red' },
    { name: 'Facebook', key: 'Facebook', icon: 'facebook f', class: 'facebook' },
    { name: 'Twitter', key: 'Twitter', icon: 'twitter', class: 'twitter' }
  ];
  loginDomainProviders: any[] = [
    { name: 'Windows', key: 'Windows', icon: 'microsoft' }
  ];

  userLock = false;

  private userLoggedSource = new BehaviorSubject<UserModel>(null);

  userLogged = this.userLoggedSource.asObservable();

  changeUserLogged(userLogged: UserModel) {
    this.userLoggedSource.next(userLogged);
  }

  constructor(
    private router: Router,
    private usersHttpService: UsersHttpService,
    private permissionsService: NgxPermissionsService,
    private oidcService: NgxMAuthOidcService) { }

  public getUserLogged(): Promise<UserModel> {
    return new Promise((resolve, reject) => {
      forkJoin(
        this.oidcService.loadUserProfile(),
        this.loadUserLogged()
      )
      .subscribe(
        ([loadUserProfile, loadUserLogged]: [any, UserModel]) => {
          const user: UserModel = {
            id: loadUserProfile.sub,
            employeeNumber: loadUserProfile.sub,
            email: loadUserProfile.email,
            userName: loadUserProfile.email,
            firstName: loadUserLogged?.firstName,
            lastName: loadUserLogged?.lastName,
            fullName: loadUserLogged?.fullName,
            phoneNumber: loadUserLogged?.phoneNumber,
            lastValidLogin: loadUserLogged?.lastValidLogin,
            lastIncorrectLogin: loadUserLogged?.lastIncorrectLogin,
            logins: loadUserLogged.logins,
            roles: [
              {
                name: 'Administrator',
                value: 'ADMINISTRATOR',
                application: {
                  name: 'epr',
                  value: 'epr'
                },
                claims: []
              }
            ],
            permissions: []
          };

          user.provider = this.loginSocialProviders.find(provider => provider.key === loadUserLogged.logins[0]);

          this.permissionsService.loadPermissions(this.permissions);

          this.changeUserLogged(user);
          resolve(user);
        },
        err => reject(err)
      );
    });
  }

  getPermissionsLogged(): Observable<PermissionsTypes[]> {
    return new Observable((observer) => observer.next(this.permissions));
  }

  public getLoggedIn(): boolean {
    return this.oidcService.hasValidAccessToken();
  }
  public getLockAccount(): boolean {
    return JSON.parse(sessionStorage.getItem('lock-account'));
  }

  public loadUserLogged() {
    return this.usersHttpService.getUserLogged();
  }

  public loginPasswordFlow(
    email: string,
    password: string): Promise<object> {

    return new Promise((resolve, reject) => {
      this.oidcService.fetchTokenUsingPasswordFlow(email, password)
        .then(token => this.getUserLogged().then(_ => resolve(token)))
        .catch(err => reject(err.error));
    });
  }

  public loginDomainFlow(
    login: string,
    password: string): Promise<object> {

    return new Promise((resolve, reject) => {
      this.oidcService.fetchTokenUsingDomainFlow(login, password)
        .then(token => this.getUserLogged().then(_ => resolve(token)))
        .catch(err => reject(err.error));
    });
  }

  public loginAuthorizationCode(providerKey) {
    const params = { provider: providerKey };
    this.oidcService.initAuthorizationCode(params);
  }


  public registerAccount(model: any) {
    return this.usersHttpService.registerAccount(model);
  }

  public reminderPassword(model: any) {
    return this.usersHttpService.reminderPassword(model);
  }

  public confirmPassword(model: any, userId: string, token: string) {
    return this.usersHttpService.confirmPassword(model, userId, token);
  }

  public confirmEmail(userId: string, token: string) {
    return this.usersHttpService.confirmEmail(userId, token);
  }

  public lockAccount(returnUrl) {
    this.router.navigate(['/account', 'lock'], { queryParams: { returnUrl } });
    sessionStorage.setItem('lock-account', JSON.stringify(true));
  }

  public unlockAccount(returnUrl) {
    this.router.navigateByUrl(returnUrl);
    sessionStorage.setItem('lock-account', JSON.stringify(false));
  }

  public logoutUser() {
    this.oidcService.logout(true);
    this.changeUserLogged(null);
    this.router.navigate(['/']);
    sessionStorage.setItem('lock-account', JSON.stringify(false));
  }
}
