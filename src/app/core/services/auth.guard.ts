import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, of } from 'rxjs';

import { AccountService } from './account.service';
import { isNullOrUndefined } from 'util';
import { map } from 'rxjs/operators';
import { PermissionsTypes } from 'src/app/shared/enums/permissions-types.enum';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

	constructor(
		private router: Router,
		private accountService: AccountService) { }

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean> {

		if (this.accountService.getLockAccount()) {
			this.router.navigate(['/account', 'lock']);
		}

		if (this.accountService.getLoggedIn()) {
			const routePermissions = route.data.permissions;
			return this.accountService.getPermissionsLogged().pipe(
				map((permissions: PermissionsTypes[]) => {
					if (!isNullOrUndefined(permissions)) {
						if (!isNullOrUndefined(routePermissions)) {
							const permissionsIntersection = routePermissions.only.filter(only =>
								permissions.map(permission => permission).includes(only));

							if (permissionsIntersection && permissionsIntersection.length > 0) {
								return true;
							}
						} else {
							return true;
						}
					}

					// access denied
					this.router.navigate(['/accessdenied'], { queryParams: { returnUrl: this.router.url } });
					return false;
				})
			);
		}

		this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
		return of(false);
	}
}
