import { Injectable, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { AppConfig } from '../../app.config';

declare var $: any;

@Injectable({ providedIn: 'root' })
export class SidebarService {
  menuBackPage: BehaviorSubject<any> = new BehaviorSubject(null);

  headerConfig: BehaviorSubject<any> = new BehaviorSubject(null);
  theme: BehaviorSubject<string> = new BehaviorSubject('style-murek');

  refreshWidgets: BehaviorSubject<any> = new BehaviorSubject(null);

  sidebarVisible = true;
  headerVisible = true;
  subheaderVisible = false;
  menuVisible = true;

  getThemes: string[] = AppConfig.settings.themes.available;
  isThemeDark = AppConfig.settings.themes.dark;
  activeTheme = AppConfig.settings.themes.default;

  implementedFunc() {
    $('.ui.modal.modal-implemented')
      .modal({
        context: 'td-layout',
        transition: 'fly left',
        closable: false,
        onDeny: () => {
        }
      })
      .modal('show');
  }
}
