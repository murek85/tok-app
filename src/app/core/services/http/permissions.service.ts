import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { AppConfig } from 'src/app/app.config';

import { BaseHttpService } from './base.service';

import { PagingSettingsModel } from 'src/app/shared/models/paging.model';
import { FilterPermissionModel } from 'src/app/shared/models/filters/filter-permission.model';

import { RoleModel } from 'src/app/shared/models/role.model';
import { ClaimModel } from 'src/app/shared/models/claim.model';

@Injectable()
export class PermissionsHttpService extends BaseHttpService {
  constructor(
    protected http: HttpClient) {

    super(http);
  }

  public getPermissions(pagingSettings: PagingSettingsModel, filters: FilterPermissionModel) {

    const params = new HttpParams({
      fromObject: {
        'pageNumber': String(pagingSettings.pageNumber),
        'pageSize': String(pagingSettings.pageSize)
      }
    });

    return this.http.post(`${AppConfig.settings.system.apiUrl}/api/permissions/list`, { ...<any>filters }, { params });
  }

  public getPermission(permissionId: number) {
    return this.http.get(`${AppConfig.settings.system.apiUrl}/api/permissions/${permissionId}`);
  }

  public createRole(role: RoleModel) {
    return this.http.post(`${AppConfig.settings.system.apiUrl}/api/permissions/role`, role);
  }

  public updateRole(role: RoleModel) {
    return this.http.put(`${AppConfig.settings.system.apiUrl}/api/permissions/role`, role);
  }

  public deleteRole(roleId: number) {
    return this.http.delete(`${AppConfig.settings.system.apiUrl}/api/permissions/role/${roleId}`);
  }

  public createClaim(claim: ClaimModel) {
    return this.http.post(`${AppConfig.settings.system.apiUrl}/api/permissions/claim`, claim);
  }

  public updateClaim(claim: ClaimModel) {
    return this.http.put(`${AppConfig.settings.system.apiUrl}/api/permissions/claim`, claim);
  }

  public deleteClaim(claimId: number) {
    return this.http.delete(`${AppConfig.settings.system.apiUrl}/api/permissions/claim/${claimId}`);
  }
}
