import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { AppConfig } from 'src/app/app.config';

import { BaseHttpService } from './base.service';

import { PagingSettingsModel } from 'src/app/shared/models/paging.model';
import { FilterUserModel } from 'src/app/shared/models/filters/filter-user.model';

@Injectable()
export class UsersHttpService extends BaseHttpService {
  constructor(
    protected http: HttpClient) {

    super(http);
  }

  public getUsers(pagingSettings: PagingSettingsModel, filters: FilterUserModel) {

    const params = new HttpParams({
      fromObject: {
        'pageNumber': String(pagingSettings.pageNumber),
        'pageSize': String(pagingSettings.pageSize)
      }
    });

    return this.http.post(`${AppConfig.settings.system.apiUrl}/api/users/list`, { ...<any>filters }, { params });
  }

  public getUser(aspId) {
    return this.http.get(`${AppConfig.settings.system.apiUrl}/api/users/${aspId}`);
  }

  public createUser(user: any) {
    return this.http.post(`${AppConfig.settings.system.apiUrl}/api/users`, user);
  }

  public updateUser(user: any) {
    return this.http.put(`${AppConfig.settings.system.apiUrl}/api/users`, user);
  }

  public deleteUser(aspId) {
    return this.http.delete(`${AppConfig.settings.system.apiUrl}/api/users/${aspId}`);
  }

  public getUserLogged()  {
    return this.http.get(`${AppConfig.settings.system.apiUrl}/api/account/user`);
  }

  public registerAccount(model: any) {
    return new Promise((resolve, reject) => {
      const params = new HttpParams();

      this.http.post(`${AppConfig.settings.system.apiUrl}/api/account/register`, model)
      .subscribe(
        result => resolve(result),
        err => reject(err.error)
      );
    });
  }

  public reminderPassword(model: any) {
    return new Promise((resolve, reject) => {
      const params = new HttpParams();

      this.http.post(`${AppConfig.settings.system.apiUrl}/api/account/password/reminder`, model, { params })
      .subscribe(
        result => resolve(result),
        err => reject(err.error)
      );
    });
  }

  public confirmPassword(model: any, userId: string, token: string) {
    return new Promise((resolve, reject) => {
      const headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('userId', userId)
        .set('token', token);

      this.http.post(`${AppConfig.settings.system.apiUrl}/api/account/password/confirm`, model, { headers })
      .subscribe(
        result => resolve(result),
        err => reject(err.error)
      );
    });
  }

  public confirmEmail(userId: string, token: string) {
    return new Promise((resolve, reject) => {
      const headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('userId', userId)
        .set('token', token);

      this.http.post(`${AppConfig.settings.system.apiUrl}/api/account/email/confirm`, {}, { headers })
      .subscribe(
        result => resolve(result),
        err => reject(err.error)
      );
    });
  }
}
