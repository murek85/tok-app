import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class BaseHttpService {

  protected headers = new HttpHeaders();

  constructor(
    protected http: HttpClient) { }
}
