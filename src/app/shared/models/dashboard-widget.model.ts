import { WidgetType } from '../enums/widget-type.enum';

export class PositionWidget {
  public top: number;
  public left: number;
  public width: number;
  public height: number;
}

export class SettingsWidget {
  public type: WidgetType;
  public header: string;
  public icon: string;
  public theme?: string;
}

export class DashboardWidget {
  public position: PositionWidget;
  public settings: SettingsWidget;
  public active: boolean;
}
