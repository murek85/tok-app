import { DashboardType } from '../enums/dashboard-type.enum';

export class DashboardConfig {
    type: DashboardType;
    title: string;
}
