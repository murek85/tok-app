import { FormFieldType } from '../enums/form-field-type.enum';

export class StepModel {
  public label: string;
  public description: string
  public symbol: string
  public segments: SegmentModel[]
  public completed = false
}

export class SegmentModel {
  public header: string;
  public description: string;
  public fields: FormFieldModel[];
}

export class FormFieldModel {
  public label: string;
  public name: string;
  public symbol: string;
  public type: FormFieldType;
  public items?: any[];
  public url?: string;
  public hints?: string[];
  public value? =  null;

  public api?: any;

  public onChange?: any;
}
