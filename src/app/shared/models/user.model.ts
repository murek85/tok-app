import { RoleModel } from './role.model';
import { PermissionsTypes } from '../enums/permissions-types.enum';

export class UserModel {
  id?: string;
  userName?: string;
  password?: string;
  passwordConfirm?: string;
  passwordNew?: string;
  email?: string;
  firstName?: string;
  lastName?: string;
  fullName?: string;
  peselNumber?: number;
  phoneNumber?: string;
  lastValidLogin?: Date;
  lastIncorrectLogin?: Date;
  roles?: RoleModel[];
  permissions?: PermissionsTypes[];
  logins?: string[];
  provider?: any;
  employeeNumber?: string;
  stats?: any[];
}
