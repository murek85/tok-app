import { PagingResultsModel } from './paging.model';

export class ResultModel<T> {
  data: T;
  paging: PagingResultsModel;
}
