export class GroupModel {
  public id?: string;
  public name?: string;
  public value?: string;
  public code?: string;
  public description?: string;
}
