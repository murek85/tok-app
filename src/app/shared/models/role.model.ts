import { ApplicationModel } from './application.model';
import { ClaimModel } from './claim.model';

export class RoleModel {
  public id?: string;
  public name?: string;
  public value?: string;
  public code?: string;
  public description?: string;
  public application?: ApplicationModel;
  public claims?: ClaimModel[];
}
