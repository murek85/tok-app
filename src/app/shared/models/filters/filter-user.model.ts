export class FilterUserModel {
  onlyUserEmailConfirmed: boolean;
  onlyUserActive: boolean;
  onlyUserTemporaryAccess: boolean;
}
