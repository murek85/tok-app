export class ErrorModel {
  public type: string;
  public message?: string;
  public desc?: string;
}
