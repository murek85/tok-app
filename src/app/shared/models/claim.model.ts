import { ApplicationModel } from './application.model';

export class ClaimModel {
  public id?: string;
  public name?: string;
  public value?: string;
  public code?: string;
  public description?: string;
  public application?: ApplicationModel;
}
