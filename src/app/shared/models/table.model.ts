import { TableType } from '../enums/table-type.enum';

export class TableSettingsModel {
  public type: TableType = TableType.List;
}
