import { ClaimModel } from './claim.model';
import { RoleModel } from './role.model';

export class PermissionModel {
  public id?: string;
  public role: RoleModel;
  public claims: ClaimModel[];
}
