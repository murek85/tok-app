export class SortObjectModel {
  name: string;
  field: string;
  order?: string | null;
}
