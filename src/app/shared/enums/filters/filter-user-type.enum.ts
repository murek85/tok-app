export enum FilterUserType {
  OnlyUserEmailConfirmed,
  OnlyUserActive,
  OnlyUserTemporaryAccess
}
