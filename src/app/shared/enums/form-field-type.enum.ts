export enum FormFieldType {
  Text          = 'Text',
  Password      = 'Password',
  Email         = 'Email',
  Number        = 'Number',
  PhoneNumber   = 'PhoneNumber',
  Single        = 'Single',
  Multi         = 'Multi',
  Search        = 'Search',
  Checkbox      = 'Checkbox',
  Checkbox2     = 'Checkbox2'
}
