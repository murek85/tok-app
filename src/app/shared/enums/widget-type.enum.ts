export enum WidgetType {
  // Pusty szablon
  Blank                   = 'blank',

  // Dashboard
  History                 = 'history',
  Holidays                = 'holidays',
  Plan                    = 'plan',
  Quotation               = 'quotation',
  Shortcut                = 'shortcut',

  // Worktimes
  MyVacation              = 'myvacation',
  MySubstitutes           = 'mysubstitutes',
  MyAbsences              = 'myabsences',
  MyHolidayPlan           = 'myholidayplan',
  MySubstitutes2           = 'mysubstitutes2',
  MyPlanAndRealization    = 'myplanandrealization',
  MyWorktime              = 'myworktime',
  MySchedules             = 'myschedules',

  // Monitor
  Cpu                     = 'cpu',
  Disk                    = 'disk',
  Applications            = 'applications'
}
