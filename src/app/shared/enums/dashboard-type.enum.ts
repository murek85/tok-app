export enum DashboardType {
  Dashboard           = 'dashboard',
  Monitor             = 'monitor',
  WorkTimes           = 'worktimes',
  Holidays            = 'holidays',
  Absences            = 'absences',
  HolidayPlan         = 'holidayplan',
  WorkTime            = 'workTime',
  Schedules           = 'schedules'
}
