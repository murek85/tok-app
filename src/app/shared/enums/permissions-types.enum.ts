export enum PermissionsTypes {
  DASHBOARD       = 'DASHBOARD',
  FINANCES        = 'FINANCES',
  HR              = 'HR',
  WORKTIMES       = 'WORKTIMES',
  USERS           = 'USERS',
  USER_CREATE     = 'USER_CREATE',
  USER_EDIT       = 'USER_EDIT',
  USER_DELETE     = 'USER_DELETE',
  PERMISSIONS     = 'PERMISSIONS',
  GROUPS          = 'GROUPS',
  LOGS            = 'LOGS',
  REPORTS         = 'REPORTS',
  MONITOR         = 'MONITOR'
}
