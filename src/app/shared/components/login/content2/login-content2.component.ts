import { Component, OnInit, Inject, AfterViewInit, OnChanges, SimpleChanges, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { TranslateService, DefaultLangChangeEvent } from '@ngx-translate/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

declare var $: any;

@Component({
  selector: 'app-login-content2',
  templateUrl: './login-content2.component.html',
  styleUrls: ['./login-content2.component.scss']
})
export class LoginContent2Component implements OnInit, OnDestroy, AfterViewInit {

  system = { version: '1.0.0', date: '21-10-2019', type: 'dev' };
  languages: any[] = AppConfig.settings.i18n.availableLanguages;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    public sidebarService: SidebarService,
    public accountService: AccountService) { }

  public ngOnInit() {
    this.system = {
      version: AppConfig.settings.system.version,
      date: AppConfig.settings.system.date,
      type: AppConfig.settings.system.type
    };
  }

  public ngOnDestroy() {
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  private initSemanticConfig() {

    this.sidebarService.isThemeDark = JSON.parse(localStorage.getItem('dark'));
    $('.checkbox-dark-theme').checkbox({
      onChecked: () => {
        this.sidebarService.isThemeDark = true; this.sidebarService.theme.next(this.sidebarService.activeTheme);
      },
      onUnchecked: () => {
        this.sidebarService.isThemeDark = false; this.sidebarService.theme.next(this.sidebarService.activeTheme);
      }
    });

    this.sidebarService.activeTheme = JSON.parse(localStorage.getItem('style'));
    $('.themes').dropdown({
      on: 'hover',
      action: 'hide',
      onChange: (value, text, $selectedItem) => {
        switch (value) {
          case 'dark':
            localStorage.setItem('dark', JSON.stringify(this.sidebarService.isThemeDark));
            break;

          default:
            localStorage.setItem('style', JSON.stringify(value));
            this.sidebarService.theme.next(value);
            this.sidebarService.activeTheme = value;
            break;
        }
      }
    });

    const language = JSON.parse(localStorage.getItem('language'));
    $('.languages').dropdown({
      on: 'hover',
      onChange: (value, text, $selectedItem) => {
        this.tdLoadingService.register();
        setTimeout(() => {
          localStorage.setItem('language', JSON.stringify(value));
          this.translateService.setDefaultLang(value);
          this.tdLoadingService.resolve();
        }, 500);
      }
    });

    $('.languages').dropdown('set selected',
      (language == null ? AppConfig.settings.i18n.defaultLanguage.code : language));
  }

  regulations() {
    $('.ui.modal.modal-regulations')
    .modal({
      context: 'td-layout',
      transition: 'fly left',
      closable: false,
      inverted: true
    })
    .modal('show');
  }

  policy() {
    $('.ui.modal.modal-policy')
    .modal({
      context: 'td-layout',
      transition: 'fly left',
      closable: false,
      inverted: true
    })
    .modal('show');
  }

  cookie() {
    $('.ui.modal.modal-cookie')
    .modal({
      context: 'td-layout',
      transition: 'fly left',
      closable: false,
      inverted: true
    })
    .modal('show');
  }
}
