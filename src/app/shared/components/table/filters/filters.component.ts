import { Component, ChangeDetectorRef, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';

import { FilterObjectModel } from 'src/app/shared/models/filter.model';
import { SortObjectModel } from 'src/app/shared/models/sort.model';

@Component({
    selector: 'app-table-filters',
    styleUrls: ['./filters.component.scss'],
    templateUrl: './filters.component.html'
})
export class FiltersComponent<T> implements OnInit, OnChanges {

  @Input() countFilter = 0;
  @Input() countSort = 0;
  @Input() @Output() showFilter = true;
  @Input() @Output() showSort = true;

  @Output() rebuildFilter: EventEmitter<any> = new EventEmitter();
  @Output() acceptFilter: EventEmitter<any> = new EventEmitter();
  @Output() clearFilter: EventEmitter<any> = new EventEmitter();
  @Output() removeFilter: EventEmitter<any> = new EventEmitter();
  @Output() changeSortOrder: EventEmitter<any> = new EventEmitter();
  @Output() quickSearch: string;

  @Input() @Output() filters: FilterObjectModel<T>[];
  @Input() @Output() sorts: SortObjectModel[];

  @Input() disableFilter = false;

  chipsfilters: any[];

  constructor(private cdr: ChangeDetectorRef) {
    this.chipsfilters = [];
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.filters && changes.filters.currentValue) {
      this.filters = changes.filters.currentValue as FilterObjectModel<T>[];

      this.chipsfilters = [];
      this.filters.forEach((filter: FilterObjectModel<T>) => {
        if (filter.value) {
          this.chipsfilters.unshift(filter);
        }
      });
    }
  }

  ngOnInit(): void {
    // obejście problemu: 'Expression has changed after it was checked'
    setTimeout(() => {
      this.calculationFilter();
      this.calculationSort();
    }, 0);
  }

  onShowFilter(): void {
    this.showFilter = !this.showFilter;
  }

  onAcceptFilter(): void {
    this.chipsfilters = [];
    this.filters.forEach((filter: FilterObjectModel<T>) => {
      if (filter.value) {
        this.chipsfilters.unshift(filter);
      }
    });

    this.showFilter = !this.showFilter;
    this.calculationFilter();
    this.acceptFilter.emit(null);
  }

  onClearFilter(): void {
    this.chipsfilters = [];
    // this.filters.forEach((filter: FilterObjectModel<T>) => {
    //   filter.active = undefined;
    //   filter.value = undefined;
    //   filter.objectsModel = [];
    // });

    this.calculationFilter();
    this.clearFilter.emit(null);
  }

  onRemoveFilter(filter: FilterObjectModel<T>): void {
    // this.filters.filter((item: FilterObjectModel<T>) => {
    //   if (filter.type === item.type) {
    //     item.active = undefined;
    //     item.value = undefined;
    //     item.objectsModel = [];
    //   }
    // });

    const idx = this.filters.findIndex(x => x.type === filter.type);
    if (idx > -1) {
      this.filters.splice(idx, 1);
    }

    this.calculationFilter();
    this.removeFilter.emit(filter);
  }

  getFilter(filter: FilterObjectModel<T>): any {
    return this.filters.filter((item: FilterObjectModel<T>) => {
      if (item === filter) {
        return item;
      }
    });
  }

  private calculationFilter(): void {
    this.countFilter = 0;
    if (this.filters) {
      this.filters.forEach(filter => {
        switch (filter.type) {
          default: {
            this.countFilter +=
              Number(filter.value != null && filter.value.toString().length > 0);
            break;
          }
        }
      });
    }

    // this.showFilter = false;
  }

  private calculationSort(): void {
    this.countSort = 0;
    if (this.sorts) {
      this.sorts.forEach((item: SortObjectModel) => {
        if (item.order) {
          this.countSort += Number(item.order.length > 0);
        }
      });
    }
  }

  private onChangeSortOrder(sort: SortObjectModel) {
    this.sorts.forEach((item: SortObjectModel) => {
      if (sort.field === item.field) {
        switch (sort.order) {
          case 'ASC':
            sort.order = 'DESC';
            break;
          case 'DESC':
            sort.order = '';
            break;

          default:
            sort.order = 'ASC';
            break;
        }
      }
    });

    this.calculationSort();
    this.changeSortOrder.emit(null);
  }
}
