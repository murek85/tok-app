import {
  Component,
  OnInit,
  OnChanges,
  AfterViewInit,
  SimpleChanges,
  SimpleChange,
  Input,
  Output,
  EventEmitter,
  ViewChild } from '@angular/core';
import { IPageChangeEvent, TdPagingBarComponent } from '@covalent/core/paging';
import { TdLoadingService } from '@covalent/core/loading';

declare var $: any;

@Component({
  selector: 'app-table-list',
  styleUrls: ['./list.component.scss'],
  templateUrl: './list.component.html'
})
export class ListComponent implements OnInit, OnChanges, AfterViewInit {

  event: IPageChangeEvent;

  @Input() countFilter = 0;
  @Input() countSort = 0;

  @Input() visibleQuickSearch: boolean;
  @Input() visibleRefresh: boolean;
  @Input() visibleFilter: boolean;
  @Input() visibleSort: boolean;
  @Input() visiblePageInfo: boolean;
  @Input() visiblePaging: boolean;
  @Input() disableFilter: boolean;
  @Input() quickSearch: any;

  @Input() data: any[];
  @Input() @Output() currentItem: any;

  @Output() selectedItem: EventEmitter<any> = new EventEmitter<any>();
  @Output() refreshData: EventEmitter<any> = new EventEmitter();
  @Output() paging: EventEmitter<any> = new EventEmitter<any>();

  @Input() acceptFilter: EventEmitter<any> = new EventEmitter();
  @Input() clearFilter: EventEmitter<any> = new EventEmitter();

  @Input() total = 0;
  @Input() @Output() initialPage = 1;

  currentPage = 1;

  @Input() @Output() pageSize = 5;
  @Input() pageSizes = [5, 10, 20, 50];
  @Input() startRefresh = true;

  @ViewChild(TdPagingBarComponent, { static: false }) pagingBar: TdPagingBarComponent;

  // konstruktor
  constructor() {
    this.visibleFilter = this.visibleSort = this.visibleRefresh = this.visiblePageInfo = this.visiblePaging = true;
    this.disableFilter = false;
  }

  // inicjalizacja komponentu
  ngOnInit(): void {
    if (this.startRefresh) {
      this.onRefreshData();
    }
  }

  ngOnChanges(changes: SimpleChanges) {

    const initialPage: SimpleChange = changes.initialPage;
    if (initialPage && this.pagingBar) {
      this.initialPage = initialPage.currentValue;
      this.pagingBar.navigateToPage(this.initialPage);
    }
  }

  ngAfterViewInit() {
    this.initSemanticConfig();
  }

  // zwraca wartość zaznaczenia elementu
  isSelected(item: any): boolean {
    if (!this.currentItem) {
      return false;
    }
    return this.currentItem === item ? true : false;
  }

  // odświeżanie danych
  onRefreshData(): void {
    // this.initialPage = 1;
    this.refreshData.emit(null);
  }

  // zdarzenie wewnętrzne komponentu
  // event selected
  onSelected(item: any): void {
    setTimeout(() => {
      this.currentItem = item;
      this.selectedItem.emit(item);
    }, 0);
  }

  // stronicowanie
  onPage(pagingEvent: IPageChangeEvent): void {
    this.paging.emit(pagingEvent);
  }

  private initSemanticConfig() {
    setTimeout(_ => {
      $('.dropdown-pagesize').dropdown({
        onChange: (value, text, $choice) => {
          this.pageSize = Number(value);
        }
      });

      $('.dropdown-pagesize').dropdown('set selected', this.pageSize);
    }, 0);
  }
}
