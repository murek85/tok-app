import { Component, Input } from '@angular/core';
import { CdkStepper } from '@angular/cdk/stepper';

declare var $: any;

@Component({
  selector: 'app-stepper-custom',
  templateUrl: './stepper-custom.component.html',
  styleUrls: ['./stepper-custom.component.scss'],
  // This custom stepper provides itself as CdkStepper so that it can be recognized
  // by other components.
  providers: [{ provide: CdkStepper, useExisting: StepperCustomComponent }]
})
export class StepperCustomComponent extends CdkStepper {

  @Input('steps') stepsList;
  @Input('error') error;

  selectedIndex = 0;

  onClick(index: number): void {
    this.selectedIndex = index;
  }
}
