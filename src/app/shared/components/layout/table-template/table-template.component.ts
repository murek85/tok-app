import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ChangeDetectorRef,
  NgZone,
  ViewChildren,
  QueryList,
  ViewChild,
  ElementRef,
  Input,
  Output,
  EventEmitter } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { IPageChangeEvent } from '@covalent/core/paging';

import { AccountService } from 'src/app/core/services/account.service';
import { UsersService } from 'src/app/components/management/users/users.service';

import { FilterUserType } from 'src/app/shared/enums/filters/filter-user-type.enum';
import { TableType } from 'src/app/shared/enums/table-type.enum';

import { UserModel } from 'src/app/shared/models/user.model';
import { FilterUserModel } from 'src/app/shared/models/filters/filter-user.model';

import { FilterObjectModel } from 'src/app/shared/models/filter.model';
import { SortObjectModel } from 'src/app/shared/models/sort.model';
import { TableSettingsModel } from 'src/app/shared/models/table.model';
import { PagingSettingsModel, PagingResultsModel } from 'src/app/shared/models/paging.model';

import { isNullOrUndefined } from 'util';

declare var $: any;

@Component({
  selector: 'app-table-template',
  templateUrl: './table-template.component.html',
  styleUrls: ['./table-template.component.scss']
})
export class TableTemplateComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input() config: any;

  @Input() data: any[];
  @Input() currentItem: any;

  @Input() @Output() filters: FilterObjectModel<any>[];
  @Input() @Output() sorts: SortObjectModel[];

  @Output() refreshData: EventEmitter<any> = new EventEmitter();

  @Output() acceptFilter: EventEmitter<any> = new EventEmitter();
  @Output() clearFilter: EventEmitter<any> = new EventEmitter();
  @Output() removeFilter: EventEmitter<any> = new EventEmitter()

  @Input() @Output() pagingResults: PagingResultsModel;

  @ViewChildren('lists') usersList: QueryList<any>;
  @ViewChild('header') headerRef: ElementRef;
  @ViewChild('filter') filterRef: ElementRef;

  tableType: typeof TableType = TableType;

  tableSettings: TableSettingsModel = new TableSettingsModel();
  pagingSettings: PagingSettingsModel = new PagingSettingsModel();

  constructor(
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService) {

    this.data = [];
    this.filters = [];
    this.sorts = [];
  }

  private initTableSettings() {
    const storage = JSON.parse(sessionStorage.getItem(`table_${this.config.type}`));
    return !isNullOrUndefined(storage) ? storage : this.tableSettings;
  }

  private initFilterSettings() {
    const storage = JSON.parse(sessionStorage.getItem(`filter_${this.config.type}`));
    return !isNullOrUndefined(storage) ? storage : this.filters;
  }

  private initPagingSettings() {
    const storage = JSON.parse(sessionStorage.getItem(`paging_${this.config.type}`));
    return !isNullOrUndefined(storage) ? storage : this.pagingSettings;
  }

  private changePagingSettings() {
    sessionStorage.setItem(`table_${this.config.type}`, JSON.stringify(this.tableSettings));
    sessionStorage.setItem(`paging_${this.config.type}`, JSON.stringify(this.pagingSettings));
    sessionStorage.setItem(`filter_${this.config.type}`, JSON.stringify(this.filters));
  }

  private initSemanticConfig() {
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    this.tableSettings = this.initTableSettings();
    this.filters = this.initFilterSettings();
    this.pagingSettings = this.initPagingSettings();
  }

  public ngOnDestroy() {
  }

  onRefreshData(): void {
    this.refreshData.emit({ pagingSettings: this.pagingSettings, filters: this.filters, sorts: this.sorts });
  }

  onPage(pagingEvent: IPageChangeEvent): void {
    this.pagingSettings.pageNumber = pagingEvent.page;
    this.pagingSettings.pageSize = pagingEvent.pageSize;

    this.changePagingSettings();
    this.onRefreshData();
  }

  onAcceptFilter(event: any) {
    this.pagingSettings.pageNumber = 1;

    this.changePagingSettings();
    this.onRefreshData();

    this.acceptFilter.emit(event);
  }

  onClearFilter(event: any) {
    this.pagingSettings.pageNumber = 1;

    this.changePagingSettings();
    this.onRefreshData();

    this.clearFilter.emit(event);

  }

  onRemoveFilter(event: any) {
    this.pagingSettings.pageNumber = 1;

    this.changePagingSettings();
    this.onRefreshData();

    this.removeFilter.emit(event);
  }

  onChangeSortOrder(event: any) {
    this.pagingSettings.pageNumber = 1;

    this.changePagingSettings();
    this.onRefreshData();
  }

  onChangeTableType(type: TableType) {
    this.tableSettings.type = type;

    this.changePagingSettings();
  }

  private createFilter(
    name: string,
    type: FilterUserType,
    field: string,
    value?: any | null) {

    const filter: FilterObjectModel<FilterUserType> = new FilterObjectModel();
    filter.name = name;
    filter.type = type;
    filter.field = field;
    filter.value = value;
    filter.objectsModel = [];

    return filter;
  }

  private createSort(
    name: string,
    field: string,
    order?: string | null) {

    const sort: SortObjectModel = new SortObjectModel();
    sort.name = name;
    sort.field = field;
    sort.order = order;

    return sort;
  }
}