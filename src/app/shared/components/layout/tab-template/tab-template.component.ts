import { Component, OnInit, AfterViewInit, Input, Output, ViewChild, OnDestroy, ChangeDetectorRef, NgZone, ChangeDetectionStrategy } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { NgxWidgetGridComponent } from 'ngx-widget-grid';
import { isNullOrUndefined } from 'util';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { DashboardConfig } from 'src/app/shared/models/dashboard-config.model';
import { DashboardWidget } from 'src/app/shared/models/dashboard-widget.model';

declare var $: any;

@Component({
  selector: 'app-tab-template',
  templateUrl: './tab-template.component.html',
  styleUrls: ['./tab-template.component.scss']
})
export class TabTemplateComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('grid', { static: true }) grid: NgxWidgetGridComponent;
  public rows = 80;
  public cols = 120;

  public swapWidgets = true;
  public showGrid = false;
  public highlightNextPosition = false;
  public editable = false;
  public gridFull = false;

  @Input() config: DashboardConfig;
  @Input() widgets: Array<DashboardWidget>;

  constructor(
    private zone: NgZone,
    private changeDetector: ChangeDetectorRef,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private sidebarService: SidebarService,
    private accountService: AccountService) { }


  private initWidgetsSettings() {
    const storage = JSON.parse(localStorage.getItem(`widgets_${this.config.type}`));
    return !isNullOrUndefined(storage) ? storage : this.widgets;
  }

  private changeWidgetsSettings() {
    localStorage.setItem(`widgets_${this.config.type}`, JSON.stringify(this.widgets));
  }

  private initSemanticConfig() {
    $('.checkbox-widgets-move').checkbox({
      onChange: () => {
        this.editable = !this.editable;
      }
    });

    $('.checkbox-widgets').checkbox({
      onChange: () => {
        this.widgets = [...this.widgets]; this.changeWidgetsSettings();
      }
    });

    $('.sidebar-widgets')
      .sidebar({
        context: $('#main')
      })
      .sidebar('setting', 'transition', 'scale down');
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    this.widgets = this.initWidgetsSettings();

    this.sidebarService.refreshWidgets.subscribe(widget => {
      this.widgets = [...this.widgets]; this.changeWidgetsSettings();
    });
  }

  public ngOnDestroy() {
    // trzeba usunąć sidebar
    $('.sidebar-widgets').sidebar('hide'); $('.sidebar-widgets').remove();

    // usuń niepotrzebną klasę CSS po elemencie 'sidebar'
    const main = document.getElementById('main'); main.classList.remove('pushable');
  }

  public sidebar() {
    $('.sidebar-widgets')
      .sidebar('show');
  }

  public changeWidget(widget: DashboardWidget) {
    widget.active = !widget.active;
  }

  public onContextMenu(event) {
  }

  public onDrawn(event) {
  }

  public onWidgetChange(event) {
    this.changeWidgetsSettings();
  }

  public onGridFull(event) {
    this.gridFull = event;
  }

  public onPositionChange(event, widget: DashboardWidget) {
    const {top, left, width, height} = event;
    if (!isNullOrUndefined(widget) && (widget.position.left !== top ||
      widget.position.top !== left ||
      widget.position.width !== width ||
      widget.position.height !== height)) {

      // nowe położenie komponentu
      widget.position.left = left;
      widget.position.top = top;
      widget.position.width = width;
      widget.position.height = height;
    }
  }
}