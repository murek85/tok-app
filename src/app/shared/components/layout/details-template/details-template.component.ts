import { Component, Input, OnInit, AfterViewInit, OnDestroy, EventEmitter, Output, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { TranslateService } from '@ngx-translate/core';
import { TdLoadingService } from '@covalent/core/loading';
import { iif } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { UserModel } from 'src/app/shared/models/user.model';
import { GroupModel } from 'src/app/shared/models/group.model';
import { ApplicationModel } from 'src/app/shared/models/application.model';
import { StepModel, FormFieldModel } from 'src/app/shared/models/step.model';

import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';

import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-details-template',
  templateUrl: './details-template.component.html',
  styleUrls: ['./details-template.component.scss']
})
export class DetailsTemplateComponent implements OnInit, OnDestroy, AfterViewInit {

  error;

  @Input() headerConfig;

  @Output() saveForm: EventEmitter<boolean> = new EventEmitter();
  @Output() backForm: EventEmitter<any> = new EventEmitter();

  userLogged: UserModel;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private formBuilder: FormBuilder,
    private sidebarService: SidebarService,
    private accountService: AccountService) {
  }

  private initSemanticConfig() {
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);
    this.accountService.getUserLogged();

    this.sidebarService.headerConfig.next(this.headerConfig);
    this.sidebarService.headerVisible = true;
    this.sidebarService.menuVisible = false;
  }

  public ngOnDestroy() {
  }

  public onBackForm() {
    this.backForm.next(null);
  }

  public onSaveForm() {
    this.saveForm.next(null);
  }
}
