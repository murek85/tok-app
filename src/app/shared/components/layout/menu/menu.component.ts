import { Component, OnInit, Inject, AfterViewInit, OnChanges, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import { TranslateService, DefaultLangChangeEvent } from '@ngx-translate/core';
import { Router, NavigationEnd, RouterStateSnapshot } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { UserModel } from 'src/app/shared/models/user.model';
import { BehaviorSubject } from 'rxjs';

declare var $: any;

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, AfterViewInit {

  @ViewChild('button', { read: ElementRef }) buttonRef: ElementRef;

  selectedMenu: any;

  userLogged: any;
  languages: any[] = AppConfig.settings.i18n.availableLanguages;
  notifications = [];

  headerConfig;

  constructor(
    private tdLoadingService: TdLoadingService,
    private router: Router,
    private accountService: AccountService,
    public sidebarService: SidebarService,
    public translateService: TranslateService) { }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);

    this.sidebarService.theme.subscribe(value => this.sidebarService.activeTheme = value);
    this.sidebarService.headerConfig.subscribe(config => this.headerConfig = config);

    this.translateService.onDefaultLangChange.subscribe(
      (params: DefaultLangChangeEvent) => {
        $('.masthead-menu-override .languages').dropdown('set selected', params.lang);
      }
    );
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public logoutUser() {
    this.accountService.logoutUser();
  }

  public lockAccount() {
    this.accountService.lockAccount(this.router.url);
  }

  private initSemanticConfig() {
    setTimeout(_ => {
      $('.masthead-menu-override .notifications').dropdown({
        on: 'hover',
        action: 'hide'
      });

      $('.masthead-menu-override .settings').dropdown({
        on: 'hover',
        action: 'hide'
      });

      this.sidebarService.activeTheme = JSON.parse(localStorage.getItem('style'));
      $('.masthead-menu-override .themes').dropdown({
        on: 'hover',
        action: 'hide',
        onChange: (value, text, $selectedItem) => {
          switch (value) {
            case 'dark':
              localStorage.setItem('dark', JSON.stringify(this.sidebarService.isThemeDark));
              break;

            default:
              localStorage.setItem('style', JSON.stringify(value));
              this.sidebarService.theme.next(value);
              this.sidebarService.activeTheme = value;
              break;
          }
        }
      });

      const language = JSON.parse(localStorage.getItem('language'));
      $('.masthead-menu-override .languages').dropdown({
        on: 'hover',
        onChange: (value, text, $selectedItem) => {
          this.tdLoadingService.register();
          setTimeout(() => {
            localStorage.setItem('language', JSON.stringify(value));
            this.translateService.setDefaultLang(value);
            this.tdLoadingService.resolve();
          }, 500);
        }
      });

      $('.masthead-menu-override .languages').dropdown('set selected',
        (language == null ? AppConfig.settings.i18n.defaultLanguage.code : language));
    }, 0);
  }

  public sidenav() {
    // this.sidebarService.sidebarVisible.next(!this.sidebarVisible) = !this.sidebarService.sidebarVisible;

    // if (this.sidebarService.sidebarVisible) {
    //   this.buttonRef.nativeElement.classList.remove('hidden');
    // } else {
    //   this.buttonRef.nativeElement.classList.add('hidden');
    // }
  }

  public contact() {
    $('.ui.modal.modal-contact')
      .modal({
        context: 'td-layout',
        transition: 'fly left',
        closable: false
      })
      .modal('show');
  }
}
