import { Component, OnInit, Inject, AfterViewInit, OnChanges, SimpleChanges, HostBinding } from '@angular/core';
import { TranslateService, DefaultLangChangeEvent } from '@ngx-translate/core';
import { Router, NavigationEnd } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { UserModel } from 'src/app/shared/models/user.model';
import { PermissionsTypes } from 'src/app/shared/enums/permissions-types.enum';

declare var $: any;

@Component({
  selector: 'app-sidenav-menu',
  templateUrl: './sidenav-menu.component.html',
  styleUrls: ['./sidenav-menu.component.scss']
})
export class SidenavMenuComponent implements OnInit, AfterViewInit {

  menus: any[];
  selectedMenu: any;

  routerLink = null;
  queryParams = null;

  userLogged: any;
  languages: any[] = AppConfig.settings.i18n.availableLanguages;

  constructor(
    private tdLoadingService: TdLoadingService,
    private router: Router,
    private accountService: AccountService,
    public sidebarService: SidebarService,
    public translateService: TranslateService) {

    this.menus = [];
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);

    this.translateService.onDefaultLangChange.subscribe(
      (params: DefaultLangChangeEvent) => {
        $('.sidenav-menu .languages').dropdown('set selected', params.lang);
      }
    );

    this.initMenus();

    this.router.events.subscribe(navigation => {
      if (navigation instanceof NavigationEnd) {
        this.selectedMenu = this.menus.find(m => navigation.url.includes(m.routerLink));
        this.initSemanticConfig();
      }
    });
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public setSelectedMenu(menu: any) {
    this.selectedMenu = menu;
  }

  public setActiveTheme(theme) {
    localStorage.setItem('style', JSON.stringify(theme));
    this.sidebarService.theme.next(theme);
    this.sidebarService.activeTheme = theme;
  }

  public initMenus() {
    this.menus.push(
      this.createMenu(
        'Dashboard',
        'Dashboard',
        'mdi mdi-view-dashboard',
        'tc-grey-50',
        'dashboard',
        true,
        [PermissionsTypes.DASHBOARD]
      )
    );

    this.menus.push(
      this.createMenu(
        'Dane kadrowe',
        'Dane kadrowe',
        'mdi mdi-account-details',
        'tc-grey-50',
        'hr',
        true,
        [PermissionsTypes.HR]
      )
    );

    this.menus.push(
      this.createMenu(
        'Urlopy i czasy pracy',
        'Urlopy i czasy pracy',
        'mdi mdi-account-clock',
        'tc-grey-50',
        'worktimes',
        true,
        [PermissionsTypes.WORKTIMES]
      )
    );

    this.menus.push(
      this.createMenu(
        'Finanse',
        'Finanse',
        'mdi mdi-account-cash',
        'tc-grey-50',
        'finances',
        true,
        [PermissionsTypes.FINANCES]
      )
    );

    this.menus.push(
      this.createMenu(
        'Zarządzanie systemem',
        'Zarządzanie systemem',
        'mdi mdi-cog',
        'tc-grey-50',
        '',
        true,
        [
          PermissionsTypes.USERS,
          PermissionsTypes.GROUPS,
          PermissionsTypes.PERMISSIONS,
          PermissionsTypes.MONITOR,
          PermissionsTypes.LOGS,
          PermissionsTypes.REPORTS
        ],
          [
          this.createMenu(
            'Użytkownicy',
            'Użytkownicy',
            'mdi mdi-account-cog',
            'tc-grey-50',
            'management/users/list',
            true,
            [PermissionsTypes.USERS]
          ),
          this.createMenu(
            'Grupy użytkowników',
            'Grupy użytkowników',
            'mdi mdi-account-group',
            'tc-grey-50',
            'management/groups/users/list',
            true,
            [PermissionsTypes.GROUPS]
          ),
          this.createMenu(
            'Grupy zasobów',
            'Grupy zasobów',
            'mdi mdi-account-group',
            'tc-grey-50',
            'management/groups/resources/list',
            true,
            [PermissionsTypes.GROUPS]
          ),
          this.createMenu(
            'Uprawnienia',
            'Uprawnienia',
            'mdi mdi-account-key',
            'tc-grey-50',
            'management/permissions/list',
            true,
            [PermissionsTypes.PERMISSIONS]
          ),
          this.createMenu(
            'Monitorowanie aplikacji',
            'Monitorowanie aplikacji',
            'mdi mdi-monitor-dashboard',
            'tc-grey-50',
            'management/monitor',
            true,
            [PermissionsTypes.MONITOR]
          ),
          this.createMenu(
            'Raporty',
            'Raporty',
            'mdi mdi-printer',
            'tc-grey-50',
            'management/reports',
            true,
            [PermissionsTypes.REPORTS]
          ),
          this.createMenu(
            'Dziennik zdarzeń',
            'Dziennik zdarzeń',
            'mdi mdi-book-open-outline',
            'tc-grey-50',
            'management/logs',
            true,
            [PermissionsTypes.LOGS]
          )
        ]
      )
    );

    // this.menus.push(
    //   this.createMenu(
    //     'Zarządzanie użytkownikami', 'Zarządzanie użytkownikami',
    //     'mdi mdi-account-multiple', 'tc-grey-50', 'management/users/list', true, []
    //   )
    // );

    // this.menus.push(
    //   this.createMenu(
    //     'Zarządzanie uprawnieniami', 'Zarządzanie uprawnieniami',
    //     'mdi mdi-account-key', 'tc-grey-50', 'management/permissions/list', true, []
    //   )
    // );

    // this.menus.push(
    //   this.createMenu(
    //     'Raporty', 'Raporty',
    //     'mdi mdi-printer', 'tc-grey-50', 'reports', true, []
    //   )
    // );

    // this.menus.push(
    //   this.createMenu(
    //     'Dziennik zdarzeń', 'Dziennik zdarzeń',
    //     'mdi mdi-book-open-outline', 'tc-grey-50', 'logs', true, []
    //   )
    // );
  }

  private createMenu(
    name: string,
    toolTip: string,
    iconClass: string,
    iconColorClass: string,
    routerLink: string,
    selected: boolean,
    permissionsOnly?: string[],
    items?: any[],
    count?: number): any {

    const menu: any = {
      name,
      toolTip,
      iconClass,
      iconColorClass,
      routerLink,
      selected,
      permissionsOnly,
      items
    };

    menu.notifications = count ? { count } : null;

    return menu;
  }

  private initSemanticConfig() {
    setTimeout(_ => {
      $('.sidenav-menu .account').dropdown({
        on: 'hover'
      });

      const language = JSON.parse(localStorage.getItem('language'));
      $('.sidenav-menu .languages').dropdown({
        on: 'hover',
        onChange: (value, text, $selectedItem) => {
          this.tdLoadingService.register();
          setTimeout(() => {
            localStorage.setItem('language', JSON.stringify(value));
            this.translateService.setDefaultLang(value);
            this.tdLoadingService.resolve();
          }, 500);
        }
      });

      $('.sidenav-menu .languages').dropdown('set selected',
        (language == null ? AppConfig.settings.i18n.defaultLanguage.code : language));
    }, 0);
  }

  public logoutUser() {
    this.accountService.logoutUser();
  }

  public lockAccount() {
    this.accountService.lockAccount(this.router.url);
  }
}
