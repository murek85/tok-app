import { Component, Input, OnInit, AfterViewInit, OnDestroy, EventEmitter, Output, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CdkStepper } from '@angular/cdk/stepper';
import { TranslateService } from '@ngx-translate/core';
import { TdLoadingService } from '@covalent/core/loading';
import { iif } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { NgxMAuthOidcService } from 'src/app/core/services/oidc/oidc.service';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { UserModel } from 'src/app/shared/models/user.model';
import { GroupModel } from 'src/app/shared/models/group.model';
import { ApplicationModel } from 'src/app/shared/models/application.model';
import { StepModel, FormFieldModel } from 'src/app/shared/models/step.model';

import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';

import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-form-template',
  templateUrl: './form-template.component.html',
  styleUrls: ['./form-template.component.scss']
})
export class FormTemplateComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input() headerConfig;
  @Input() steps: StepModel[];

  @Output() saveForm: EventEmitter<boolean> = new EventEmitter();
  @Output() backForm: EventEmitter<any> = new EventEmitter();
  @Output() changeStep: EventEmitter<CdkStepper> = new EventEmitter();
  @Output() previousStep: EventEmitter<CdkStepper> = new EventEmitter();
  @Output() nextStep: EventEmitter<CdkStepper> = new EventEmitter();

  @ViewChild('stepper') stepper: CdkStepper;

  userLogged: UserModel;
  error;
  selectedIndexStep = 0;

  formFieldType: typeof FormFieldType = FormFieldType;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private formBuilder: FormBuilder,
    private oidcService: NgxMAuthOidcService,
    private sidebarService: SidebarService,
    private accountService: AccountService) {

    this.steps = [];
  }

  private initSemanticConfig() {
    this.steps.forEach(step => this.initForm(step));
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);
    this.accountService.getUserLogged();

    this.sidebarService.headerConfig.next(this.headerConfig);
    this.sidebarService.headerVisible = false;
    this.sidebarService.subheaderVisible = false;
    this.sidebarService.menuVisible = false;
  }

  public ngOnDestroy() {
  }

  public onChangeStep($event) {
    this.error = null;

    const stepper = $event as CdkStepper;
    const $form = $(`.ui.form-${this.headerConfig.symbol}`);

    this.steps[stepper.selectedIndex].completed = $form.form('validate form');
    this.changeStep.next($event);
  }

  public onPreviousStep($event) {
    this.error = null;

    const stepper = $event as CdkStepper;
    const $form = $(`.ui.form-${this.headerConfig.symbol}`);

    this.steps[stepper.selectedIndex].completed = $form.form('validate form');
    stepper.previous();
    this.initForm(this.steps[stepper.selectedIndex]);
    this.previousStep.next($event);
  }

  public onNextStep($event) {
    this.error = null;

    const stepper = $event as CdkStepper;
    const $form = $(`.ui.form-${this.headerConfig.symbol}`);

    this.steps[stepper.selectedIndex].completed = $form.form('validate form');
    console.log($form.form('get values'));
    if ($form.form('validate form')) {
      stepper.next();
      this.initForm(this.steps[stepper.selectedIndex]);
      this.nextStep.next($event);
    }
  }

  public onBackForm() {
    this.backForm.next(null);
  }

  public onSaveForm($event) {
    const flatten = arr => arr.reduce(
      (a, b) => a.concat(Array.isArray(b) ? flatten(b) : b), []
    );

    this.error = null;

    const stepper = $event as CdkStepper;
    const $form = $(`.ui.form-${this.headerConfig.symbol}`);
    console.log($form.form('get values'));
    this.steps[stepper.selectedIndex].completed = $form.form('validate form');

    const completed: boolean[] = flatten(this.steps
      .map(step => {
        return step.completed;
      }))
    if (!completed.every(value => value)) {
      return;
    }

    this.tdLoadingService.register('listSyntax');

    const fields = flatten(this.steps
      .map(step => {
        return step.segments
          .map(segment => {
            return segment.fields;
          });
      }))
      .map(field => {
        return {
          name: field.name,
          value: field.value
        };
      }
    );
    console.log(fields);

    const model = fields.reduce((map, obj) => {
      map[obj.name] = obj.value;
      return map;
    }, {});
    console.log(model);

    this.saveForm.next(model);
    this.tdLoadingService.resolve('listSyntax');
  }

  private initForm(step: StepModel) {
    step.segments.forEach(segment => {
      segment.fields.forEach(field => {
        switch (field.type) {
          case FormFieldType.Checkbox: {
            setTimeout(_ => {
              $(`.checkbox-${field.symbol}`).checkbox({
                onChecked: () => {
                  field.value = true;
                },
                onUnchecked: () => {
                  field.value = false;
                }
              });
            }, 0);
            break;
          }
          case FormFieldType.Checkbox2: {
            field.items.forEach(item => {
              setTimeout(_ => {
                $(`.checkbox-${item.symbol}`).checkbox({
                  onChecked: () => {
                  },
                  onUnchecked: () => {
                  }
                });
              }, 0);
            });
            break;
          }
          case FormFieldType.Email:
          case FormFieldType.Number:
          case FormFieldType.PhoneNumber:
          case FormFieldType.Password:
          case FormFieldType.Text: {
            field.onChange = (model: FormFieldModel) => {
              const $form = $(`.ui.form-${this.headerConfig.symbol}`);
              model.value = $form.form('get value', model.name);
            }
            break;
          }
          case FormFieldType.Multi: {
            setTimeout(_ => {
              $(`.dropdown-${field.symbol}`).dropdown({
                values: field.items,
                placeholder: 'Wybierz',
                message: {
                  addResult: 'Dodaj <b>{term}</b>',
                  count: 'Zaznaczono {count}',
                  maxSelections: `Maksymalnie {maxCount}`,
                  noResults: 'Nie znaleziono żadnych wpisów.'
                },
                onChange: (value, text, $selectedItem) => {
                  field.value = value;
                }
              });

              $(`.dropdown-${field.symbol}`).dropdown('set selected', field.value ? field.value.split(',') : null);
            }, 0);
            break;
          }
          case FormFieldType.Single: {
            setTimeout(_ => {
              $(`.dropdown-${field.symbol}`).dropdown({
                values: field.items,
                placeholder: 'Wybierz',
                message: {
                  addResult: 'Dodaj <b>{term}</b>',
                  count: 'Zaznaczono {count}',
                  maxSelections: `Maksymalnie {maxCount}`,
                  noResults: 'Nie znaleziono żadnych wpisów.'
                },
                showOnFocus: false,
                onChange: (value, text, $selectedItem) => {
                  field.value = value;
                }
              });

              $(`.dropdown-${field.symbol}`).dropdown('set selected', field.value);
            }, 0);
            break;
          }
          case FormFieldType.Single && FormFieldType.Search: {
            setTimeout(_ => {
              $(`.dropdown-${field.symbol}`).dropdown({
                placeholder: 'Wybierz',
                message: {
                  addResult: 'Dodaj <b>{term}</b>',
                  count: 'Zaznaczono {count}',
                  maxSelections: `Maksymalnie {maxCount}`,
                  noResults: 'Nie znaleziono żadnych wpisów.'
                },
                apiSettings: {
                  url: field.api.url,
                  cache: false,
                  beforeXHR: (xhr) => {
                    xhr.setRequestHeader('Authorization', this.oidcService.authorizationHeader());
                    return xhr;
                  },
                  onResponse: field.api.onResponse
                },
                minCharacters: field.api.minCharacters,
                fields: field.api.fields,
                showOnFocus: false,
                onChange: (value, text, $selectedItem) => {
                  field.value = value;
                }
              });

              $(`.dropdown-${field.symbol}`).dropdown('set selected', field.value);
            }, 0);
            break;
          }
        }
      });
    })
  }
}
