import { Component, OnInit, Inject, AfterViewInit, OnChanges, SimpleChanges, ViewChild, ElementRef, HostBinding, ChangeDetectorRef, AfterContentChecked, AfterViewChecked } from '@angular/core';
import { TranslateService, DefaultLangChangeEvent } from '@ngx-translate/core';
import { Router, NavigationEnd, RouterStateSnapshot } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { UserModel } from 'src/app/shared/models/user.model';

import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { OverlayContainer } from '@angular/cdk/overlay';
import { map } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { isNullOrUndefined } from 'util';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {

  @ViewChild('button', { read: ElementRef }) buttonRef: ElementRef;

  selectedMenu: any;

  userLogged: any;
  languages: any[] = AppConfig.settings.i18n.availableLanguages;
  notifications = [];

  headerConfig;
  showQuestion = true;
  showMenuBackPage = false;
  routerLink = null;
  queryParams = null;

  constructor(
    private tdLoadingService: TdLoadingService,
    private router: Router,
    private accountService: AccountService,
    public sidebarService: SidebarService,
    public translateService: TranslateService) { }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);

    this.sidebarService.theme.subscribe(value => this.sidebarService.activeTheme = value);
    this.sidebarService.headerConfig.subscribe(config => this.headerConfig = config);

    this.sidebarService.sidebarVisible = true;

    this.sidebarService.menuBackPage.subscribe(result => {
      if (!isNullOrUndefined(result)) {
        this.showMenuBackPage = result.show;
        this.routerLink = result.routerLink;
        this.queryParams = result.queryParams;
        this.showQuestion = result.question;
      }
    });

    this.translateService.onDefaultLangChange.subscribe(
      (params: DefaultLangChangeEvent) => {
        $('.masthead-header .languages').dropdown('set selected', params.lang);
      }
    );
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  private initSemanticConfig() {
    setTimeout(_ => {
      $('.masthead-header .notifications').dropdown({
        on: 'hover',
        action: 'hide'
      });

      $('.masthead-header .settings').dropdown({
        on: 'hover',
        action: 'hide'
      });

      this.sidebarService.isThemeDark = JSON.parse(localStorage.getItem('dark'));
      $('.checkbox-dark-theme').checkbox({
        onChecked: () => {
          this.sidebarService.isThemeDark = true; this.sidebarService.theme.next(this.sidebarService.activeTheme);
        },
        onUnchecked: () => {
          this.sidebarService.isThemeDark = false; this.sidebarService.theme.next(this.sidebarService.activeTheme);
        }
      });

      this.sidebarService.activeTheme = JSON.parse(localStorage.getItem('style'));
      $('.masthead-header .themes').dropdown({
        on: 'hover',
        action: 'hide',
        onChange: (value, text, $selectedItem) => {
          switch (value) {
            case 'dark':
              localStorage.setItem('dark', JSON.stringify(this.sidebarService.isThemeDark));
              break;

            default:
              localStorage.setItem('style', JSON.stringify(value));
              this.sidebarService.theme.next(value);
              this.sidebarService.activeTheme = value;
              break;
          }
        }
      });

      const language = JSON.parse(localStorage.getItem('language'));
      $('.masthead-header .languages').dropdown({
        on: 'hover',
        onChange: (value, text, $selectedItem) => {
          this.tdLoadingService.register();
          setTimeout(() => {
            localStorage.setItem('language', JSON.stringify(value));
            this.translateService.setDefaultLang(value);
            this.tdLoadingService.resolve();
          }, 500);
        }
      });

      $('.masthead-header .languages').dropdown('set selected',
        (language == null ? AppConfig.settings.i18n.defaultLanguage.code : language));
    }, 0);
  }

  public sidenav() {
    this.sidebarService.sidebarVisible = !this.sidebarService.sidebarVisible;
    // if (this.sidebarService.sidebarVisible) {
    //   this.buttonRef.nativeElement.classList.remove('full');
    // } else {
    //   this.buttonRef.nativeElement.classList.add('full');
    // }
  }

  public back() {
    this.router.navigate([this.routerLink], { queryParams: this.queryParams });
    this.sidebarService.menuBackPage.next({ show: false, routerLink: null, queryParams: null });
  }

  public logoutUser() {
    this.accountService.logoutUser();
  }

  public lockAccount() {
    this.accountService.lockAccount(this.router.url);
  }

  public contact() {
    $('.ui.modal.modal-contact')
      .modal({
        context: 'td-layout',
        transition: 'fly left',
        closable: false
      })
      .modal('show');
  }
}
