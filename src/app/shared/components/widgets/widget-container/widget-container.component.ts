import { Component, OnInit, AfterViewInit, Input, Output } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';

import { WidgetType } from 'src/app/shared/enums/widget-type.enum';

declare var $: any;

@Component({
  selector: 'app-widget-container',
  templateUrl: './widget-container.component.html'
})
export class WidgetContainerComponent implements OnInit, AfterViewInit {

	@Input() widget;

	widgetType: typeof WidgetType = WidgetType;

	constructor(
		private tdLoadingService: TdLoadingService,
		private translateService: TranslateService,
		private accountService: AccountService) { }

	private initSemanticConfig() {
	}

	public ngAfterViewInit() {
		this.initSemanticConfig();
	}

	public ngOnInit() {
	}
}