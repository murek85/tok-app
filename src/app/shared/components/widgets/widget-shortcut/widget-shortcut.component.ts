import { Component, OnInit, AfterViewInit, Input, Output } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

declare var $: any;

@Component({
  selector: 'app-widget-shortcut',
  templateUrl: './widget-shortcut.component.html',
  styleUrls: ['./widget-shortcut.component.scss']
})
export class WidgetShortcutComponent implements OnInit, AfterViewInit {

  @Input() widget;

  buttons = [
    { header: 'Wniosek urlopowy', icon: 'plus', type: 'holiday' },
    { header: 'Plan urlopów', icon: 'umbrella beach', routerLink: '/worktimes/holiday-plan' },
    { header: 'Kartoteka płacowa', icon: 'money check alternate', routerLink: '/finances' },
    { header: 'Historia operacji', icon: 'history', routerLink: '/account/profile/history' },
    { header: 'Zmień hasło', icon: 'lock', routerLink: '/account/profile/security' },
    { header: 'Instrukcja obsługi', icon: 'book', type: 'instr' }
  ];

  constructor(
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private accountService: AccountService,
    private sidebarService: SidebarService) { }

  private initSemanticConfig() {
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
  }

  public clickFunc(type: string) {
    switch (type) {
      case 'holiday':
      case 'instr':
        this.sidebarService.implementedFunc();
        break;
    }
  }
}