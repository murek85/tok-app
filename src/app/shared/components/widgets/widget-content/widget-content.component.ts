import { Component, OnInit, AfterViewInit, Input, Output, OnDestroy } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { DashboardWidget } from 'src/app/shared/models/dashboard-widget.model';

declare var $: any;

@Component({
  selector: 'app-widget-content',
  template: `
    <div class="widget-content">
      <div class="ui basic segment widget-header"
        [ngClass]="{
          'bgc-grey-50': widget.settings.theme !== 'none',
          'widget': widget.settings.theme !== 'none',
          'segment-shadow': widget.settings.theme !== 'none'
        }"
        style="margin: 0;
          z-index: 1;
          box-shadow: 0 -10px 25px -10px rgba(0,0,0,.1) !important;">

        <div class="ui basic segment" layout="row" layout-align="center center" style="padding: 0 0 .5em 0;">
          <div layout="column" layout-align="start start">
            <h4 class="ui header">
                <i *ngIf="widget.settings.icon" class="{{widget.settings.icon}} icon"></i>
                <div class="content">
                    <span>{{widget.settings.header | translate}}</span>
                </div>
            </h4>
          </div>

          <div layout="column" layout-align="start end" flex>
            <div layout="row">
              <div class="ui icon top right pointing dropdown circular button settings"
                style="box-shadow: 0px 0px 25px 5px rgba(0,0,0,.1);">

                <i class="cog icon settings-icon"></i>

                <div class="menu">
                  <div class="item" (click)="edit()">
                    <i class="pen icon"></i>
                    <span translate>Ustawienia</span>
                  </div>
                  <div class="item" (click)="delete()">
                    <i class="trash alternate red icon"></i>
                    <span translate>Usuń</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="ui basic segment"
        style="margin: 0; padding: 0; height: calc(100% - 85px); overflow-y: auto; overflow-x: hidden;"
        [ngClass]="{
        }">

        <ng-content select="[content]"></ng-content>
      </div>
      <div class="ui divider"
        style="padding-bottom: 1em; border-width: .2em;"
        [ngStyle]="{
          'margin-top': (widget.settings.theme === 'none' ? '1em' : '0')
        }"></div>
    </div>

    <!-- ustawienia komponentu -->
    <div class="ui tiny modal modal-{{widget.settings.type}}-edit">
      <i class="close icon"></i>
      <div class="content">
        <form class="ui medium form form-{{widget.settings.type}}-edit">
          <div class="ui basic">
            <h2 class="ui header">
              <i class="pencil icon"></i>
              <div class="content">
                <span translate>Ustawienia</span>
                <div class="sub header">
                  <span class="pad-left-xs" translate>ustawienia komponentu '{{widget.settings.header}}'</span>
                </div>
              </div>
            </h2>

            <div class="size-height-12"></div>

            <!--<p class="ui text" translate>Modyfikacja ustawień komponentu.</p>

            <div class="size-height-12"></div>-->

            <div class="ui column middle aligned grid">
              <div class="row">
                <div class="six wide column">
                  <i class="small hashtag icon" style="line-height: .9; font-size: 1em;"></i>
                  <span class="pad-xs"></span>
                  <span class="tc-grey-600">{{'Nazwa komponentu' | translate}}</span>
                </div>
                <div class="ten wide column">
                  <div class="field">
                    <input [(ngModel)]="widget.settings.header"
                        type="text" name="widgetHeader" placeholder="{{'Wprowadź' | translate}}">
                  </div>
                </div>
              </div>
              <!--<div class="ui divider" style="margin: 0; border-width: .1em;"></div>
              <div class="row">
                <div class="six wide column">
                  <i class="small hashtag icon" style="line-height: .9; font-size: 1em;"></i>
                  <span class="pad-xs"></span>
                  <span class="tc-grey-600">{{'Powtórz hasło' | translate}}</span>
                </div>
                <div class="ten wide column">
                  <div class="field">
                    <input [(ngModel)]="user.passwordConfirm"
                        type="password" name="passwordConfirm" placeholder="{{'Wprowadź' | translate}}">
                  </div>
                </div>
              </div>-->
            </div>

          </div>
        </form>
      </div>

      <div class="actions">
        <button class="ui cancel button">
          <span translate>Anuluj</span>
        </button>

        <button class="ui primary approve button" style="box-shadow: 0px 0px 25px 5px rgba(0,0,0,.1);">
          <span translate>Zapisz</span>
        </button>
      </div>
    </div>

    <!-- usuwanie komponentu -->
    <div class="ui tiny modal modal-{{widget.settings.type}}-delete">
      <i class="close icon"></i>
      <div class="content">
        <form class="ui medium form form-{{widget.settings.type}}-delete">
          <div class="ui basic">
            <h2 class="ui header">
              <i class="trash red icon"></i>
              <div class="content">
                <span translate>Usuń</span>
                <div class="sub header">
                  <span class="pad-left-xs" translate>usuwanie komponentu '{{widget.settings.header}}'</span>
                </div>
              </div>
            </h2>

            <div class="size-height-12"></div>

            <p class="ui text" translate>Czy na pewno potwierdzasz usunięcie wybranego komponentu '{{widget.settings.header}}' z aktualnego pulpitu?</p>

            <div class="size-height-12"></div>
          </div>
        </form>
      </div>

      <div class="actions">
        <button class="ui cancel button">
          <span translate>Anuluj</span>
        </button>

        <button class="ui red approve button" style="box-shadow: 0px 0px 25px 5px rgba(0,0,0,.1);">
          <span translate>Usuń</span>
        </button>
      </div>
    </div>
  `,
  styles: [
    `.settings {
       background: transparent;
       box-shadow: 0px 0px 25px 5px rgba(0,0,0,.1);
     }
     .segment-shadow {
       box-shadow: 0px 0px 25px 5px rgba(0,0,0,.1) !important;
     }
    }`
  ]
})
export class WidgetContentComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input() widget: DashboardWidget;

  constructor(
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private sidebarService: SidebarService,
    private accountService: AccountService) { }

  private initSemanticConfig() {
    $('.settings').dropdown({
      action: 'hide'
    });
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
  }

  public ngOnDestroy() {
    $(`.ui.modal.modal-${this.widget.settings.type}-delete`).remove();
    $(`.ui.modal.modal-${this.widget.settings.type}-edit`).remove();
  }

  delete() {
    $(`.ui.modal.modal-${this.widget.settings.type}-delete`)
      .modal({
        context: 'td-layout',
        transition: 'fly left',
        closable: false,
        onDeny: () => {
        },
        onApprove: () => {
          this.widget.active = false; this.sidebarService.refreshWidgets.next(null);
        }
      })
      .modal('show');
  }

  edit() {
    $(`.ui.modal.modal-${this.widget.settings.type}-edit`)
      .modal({
        context: 'td-layout',
        transition: 'fly left',
        closable: false,
        onDeny: () => {
        },
        onApprove: () => {
        }
      })
      .modal('show');
  }
}
