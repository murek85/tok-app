import { Component, OnInit, AfterViewInit, Input, Output } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';

declare var $: any;

@Component({
  selector: 'app-widget-holidays',
  templateUrl: './widget-holidays.component.html',
  styleUrls: ['./widget-holidays.component.scss']
})
export class WidgetHolidaysComponent implements OnInit, AfterViewInit {

  @Input() widget;

  gauges = [
    { type: 'arch', value: 1, max: 26, label: 'Urlop taryfowy', appendText: '', foregroundColor: 'rgba(124,179,66,1)' },
    { type: 'arch', value: 23, max: 26, label: 'Urlop zaległy', appendText: '', foregroundColor: 'rgba(229,57,53 ,1)' },
    { type: 'arch', value: 3, max: 4, label: 'Urlop na żądanie', appendText: '', foregroundColor: 'rgba(253,216,53,1)' },
    { type: 'arch', value: 40, max: 40, label: 'Nadgodziny', appendText: '', foregroundColor: 'rgba(253,216,53,1)' }
  ];

  constructor(
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private accountService: AccountService) { }

  private initSemanticConfig() {
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
  }
}