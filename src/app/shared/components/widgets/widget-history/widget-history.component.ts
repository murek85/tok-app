import { Component, OnInit, AfterViewInit, Input, Output } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';

declare var $: any;

@Component({
  selector: 'app-widget-history',
  templateUrl: './widget-history.component.html',
  styleUrls: ['./widget-history.component.scss']
})
export class WidgetHistoryComponent implements OnInit, AfterViewInit {

	@Input() widget;

	constructor(
		private tdLoadingService: TdLoadingService,
		private translateService: TranslateService,
		private accountService: AccountService) { }

	private initSemanticConfig() {
	}

	public ngAfterViewInit() {
		this.initSemanticConfig();
	}

	public ngOnInit() {
	}
}