import { Component, OnInit, AfterViewInit, Input, Output } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';

declare var $: any;

const QUOTATIONS = [
  { title: 'Nigdy nie ma się drugiej okazji, żeby zrobić pierwsze wrażenie.', author: 'Andrzej Sapkowski' },
  { title: 'Przyjaciel to człowiek, który wie wszystko o tobie i wciąż cię lubi.', author: 'Elbert Hubbard' },
  { title: 'Nie każdy bliski jest bliski, nie każdy daleki – daleki.', author: 'Talmud' },
  { title: 'Nie jestem szalony, interesuje mnie wolność.', author: 'Jim Morrison' },
  { title: 'By dojść do źródła, trzeba płynąć pod prąd.', author: 'Stanisław Jerzy Lec' },
  { title: 'Poza tym, czy tego chcemy czy nie, zawsze na coś czekamy.', author: 'Lolita Pille' }
];

@Component({
  selector: 'app-widget-quotation',
  templateUrl: './widget-quotation.component.html',
  styleUrls: ['./widget-quotation.component.scss']
})
export class WidgetQuotationComponent implements OnInit, AfterViewInit {

  @Input() widget;

  quotation = QUOTATIONS[this.getRandomIntInclusive(0, QUOTATIONS.length)];

  constructor(
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private accountService: AccountService) { }

  private initSemanticConfig() {
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    this.quotation = QUOTATIONS[this.getRandomIntInclusive(0, QUOTATIONS.length)];
  }

  private getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}