import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthComponent } from './components/account/auth/auth.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: './components/account/login/login.module#LoginModule' },
  { path: 'finances', loadChildren: './components/finances/finances.module#FinancesModule' },
  { path: 'hr', loadChildren: './components/hr/hr.module#HrModule' },
  { path: 'worktimes', loadChildren: './components/worktimes/worktimes.module#WorktimesModule' },
  { path: 'accessdenied', loadChildren: './components/accessdenied/accessdenied.module#AccessDeniedModule' },
  { path: 'notfound', loadChildren: './components/notfound/notfound.module#NotFoundModule' },
  { path: 'auth', component: AuthComponent },
  { path: '**', redirectTo: 'notfound' }
];

export const routing = RouterModule.forRoot(routes);

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }
