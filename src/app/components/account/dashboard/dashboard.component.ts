import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { NgxWidgetGridComponent } from 'ngx-widget-grid';
import { isNullOrUndefined } from 'util';
import { Subject } from 'rxjs';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { DashboardConfig } from 'src/app/shared/models/dashboard-config.model';
import { DashboardWidget } from 'src/app/shared/models/dashboard-widget.model';
import { UserModel } from 'src/app/shared/models/user.model';

import { DashboardType } from 'src/app/shared/enums/dashboard-type.enum';
import { WidgetType } from 'src/app/shared/enums/widget-type.enum';

declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit {

  userLogged: UserModel;

  system = { version: '1.0.0', date: '21-10-2019', type: 'dev', appName: 'Nazwa aplikacji' };
  languages: any[] = AppConfig.settings.i18n.availableLanguages;

  notifications = [];

  headerConfig = {
    title: 'Dashboard',
    meta: '',
    icon: {
      name: '',
      color: ''
    }
  };

  config: DashboardConfig = {
    type: DashboardType.Dashboard,
    title: 'Dashboard'
  };
  widgets: Array<DashboardWidget> = [
  {
    position: {
      top: 1,
      left: 1,
      height: 58,
      width: 80
    },
    settings: {
      type: WidgetType.Plan,
      header: 'Urlopy i czas pracy',
      icon: 'calendar alternate outline'
    },
    active: true
  },
  {
    position: {
      top: 59,
      left: 1,
      height: 22,
      width: 80
    },
    settings: {
      type: WidgetType.History,
      header: 'Historia operacji',
      icon: 'history',
      theme: 'none'
    },
    active: true
  },
  {
    position: {
      top: 1,
      left: 81,
      height: 29,
      width: 40
    },
    settings: {
      type: WidgetType.Holidays,
      header: 'Limity urlopowe',
      icon: 'tasks',
      theme: 'none'
    },
    active: true
  },
  {
    position: {
      top: 30,
      left: 81,
      height: 29,
      width: 40
    },
    settings: {
      type: WidgetType.Shortcut,
      header: 'Co chcesz zrobić?',
      icon: 'question circle outline',
      theme: 'none'
    },
    active: true
  },
  {
    position: {
      top: 59,
      left: 81,
      height: 22,
      width: 40
    },
    settings: {
      type: WidgetType.Quotation,
      header: 'Cytat dnia',
      icon: 'quote left',
      theme: 'none'
    },
    active: true
  }
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private sidebarService: SidebarService,
    public accountService: AccountService) {
  }

  private initSemanticConfig() {
    $('.settings').dropdown({
      action: 'hide'
    });
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);

    this.sidebarService.headerConfig.next(this.headerConfig);
    this.sidebarService.headerVisible = true;
    this.sidebarService.menuVisible = true;

    this.system = {
      version: AppConfig.settings.system.version,
      date: AppConfig.settings.system.date,
      type: AppConfig.settings.system.type,
      appName: AppConfig.settings.system.appName
    };
  }
}
