import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

declare var $: any;

@Component({
  selector: 'app-email-confirm',
  templateUrl: './email-confirm.component.html',
  styleUrls: ['./email-confirm.component.scss']
})
export class EmailConfirmComponent implements OnInit, AfterViewInit {

  success;
  error;

  returnUrl: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    public sidebarService: SidebarService,
    public accountService: AccountService) { }

  private initSemanticConfig() {
    $('.ui.form-email').form({
      fields: {
      }
    });
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    if (this.accountService.getLoggedIn()) {
      this.router.navigate(['/dashboard']);
    } else {
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
    }
  }

  public confirmEmail() {
    this.error = null; this.success = null;

    if (!$('.ui.form-email').form('is valid')) {
      return;
    }

    this.tdLoadingService.register();
    const userId = this.route.snapshot.queryParams['userId'];
    const token = this.route.snapshot.queryParams['token'];
    this.accountService.confirmEmail(userId, token)
    .then(result => {
      // $('body').toast({
      //   title: 'Konto potwierdzone',
      //   message: 'Twoje nowe konto użytkownika zostało pomyślnie potwierdzeone.',
      //   showProgress: 'bottom',
      //   progressUp: true
      // });
      this.success = {
        message: 'Twoje nowe konto użytkownika zostało pomyślnie potwierdzone w aplikacji.'
      };
      console.log(result);
    })
    .catch(err => {
      console.error(err);
      this.error = err;
    })
    .finally(() => {
      this.tdLoadingService.resolve();
    });
  }
}
