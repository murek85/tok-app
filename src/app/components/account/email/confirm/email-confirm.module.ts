import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { EmailConfirmComponent } from './email-confirm.component';
import { EmailConfirmRoutingModule } from './email-confirm-routing.module';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    EmailConfirmComponent
  ],
  imports: [
    EmailConfirmRoutingModule,

    APP_MODULES
  ],
  providers: []
})
export class EmailConfirmModule { }
