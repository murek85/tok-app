import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

declare var $: any;

@Component({
  selector: 'app-register',
  templateUrl: './register2.component.html',
  styleUrls: ['./register2.component.scss']
})
export class RegisterComponent implements OnInit, AfterViewInit {

  email;
  userName;
  firstName;
  lastName
  newPassword;
  repeatPassword;
  success;
  error;

  returnUrl: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    public sidebarService: SidebarService,
    public accountService: AccountService) { }

  private initSemanticConfig() {
    $('.ui.form-register').form({
      fields: {
        userName: {
          identifier: 'userName',
          rules: [
            {
              type: 'empty',
              prompt: `Pole '{name}' nie może być puste.`
            }
          ]
        },
        email: {
          identifier: 'email',
          rules: [
            {
              type: 'email',
              prompt: `Pole '{name}' wymaga wprowadzenia poprawnego adresu e-mail.`
            }
          ]
        },
        newPassword: {
          identifier: 'newPassword',
          rules: [
            {
              type: 'minLength[6]',
              prompt: `Pole '{name}' wymaga wprowadzenia minimum {ruleValue} znaków.`
            }
          ]
        },
        repeatPassword: {
          identifier: 'repeatPassword',
          rules: [
            {
              type: 'minLength[6]',
              prompt: `Pole '{name}' wymaga wprowadzenia minimum {ruleValue} znaków.`
            }
          ]
        },
        agree: {
          identifier: 'agree',
          rules: [
            {
              type: 'checked',
              prompt: `Pole '{name}' należy zaakceptować.`
            }
          ]
        }
      },
      inline: false,
      on: 'blur'
    });
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    if (this.accountService.getLoggedIn()) {
      this.router.navigate(['/dashboard']);
    } else {
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
    }
  }

  public register() {
    this.error = null; this.success = null;

    if (!$('.ui.form-register').form('is valid')) {
      return;
    }

    const model = {
      userName: this.userName,
      email: this.email,
      firstName: this.firstName,
      lastName: this.lastName,
      newPassword: this.newPassword,
      repeatPassword: this.repeatPassword
    };
    this.accountService.registerAccount(model)
    .then(result => {
      // $('body').toast({
      //   title: 'Utworzono konto',
      //   message: 'Pomyślnie utworzono Twoje nowe konto użytkownika w aplikacji.',
      //   showProgress: 'bottom',
      //   progressUp: true
      // });
      this.success = {
        message: 'Pomyślnie utworzono Twoje nowe konto użytkownika w aplikacji. Na podany adres e-mail została wysłana wiadomość z linkiem umożliwiającym potwierdzenie utworzenia konta.'
      };
      console.log(result);
    })
    .catch(err => {
      console.error(err);
      this.error = err;
    })
    .finally(() => {
      this.tdLoadingService.resolve()
    });
  }
}
