import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LockComponent } from './lock.component';

import { AuthGuard } from 'src/app/core/services/auth.guard';

const routes: Routes = [
  {
    path: 'account', children: [
      { path: 'lock', component: LockComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class LockRoutingModule { }
