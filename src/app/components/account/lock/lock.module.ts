import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { LockComponent } from './lock.component';
import { LockRoutingModule } from './lock-routing.module';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    LockComponent
  ],
  imports: [
    LockRoutingModule,

    APP_MODULES
  ],
  providers: []
})
export class LockModule { }
