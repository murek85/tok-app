import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { UserModel } from 'src/app/shared/models/user.model';
import { SidebarService } from 'src/app/core/services/sidebar.service';

declare var $: any;

@Component({
  selector: 'app-lock',
  templateUrl: './lock2.component.html',
  styleUrls: ['./lock2.component.scss']
})
export class LockComponent implements OnInit, AfterViewInit {

  userLogged: UserModel;

  userName;
  password;
  email;
  error;

  returnUrl: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    public sidebarService: SidebarService,
    public accountService: AccountService) { }

  private initSemanticConfig() {
    $('.ui.form-lock').form({
      fields: {
        password: {
          identifier: 'password',
          rules: [
            {
              type: 'minLength[6]',
              prompt: 'Pole {name} wymaga wprowadzenia minimum {ruleValue} znaków.'
            }
          ]
        }
      },
      inline: false,
      on: 'blur'
    });
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';

    if (!this.accountService.getLoggedIn()) {
      this.router.navigateByUrl(this.returnUrl);
    }
  }

  public logoutUser() {
    this.accountService.logoutUser();
  }

  public unlockAccount() {
    this.error = null;

    if (!$('.ui.form-lock').form('is valid')) {
      return;
    }

    this.tdLoadingService.register();
    this.accountService.loginPasswordFlow(this.userLogged.userName, this.password)
    .then(result => {
      this.accountService.unlockAccount(this.returnUrl);
    })
    .catch(err => {
      console.error(err);
      this.error = err;
    }).finally(() => {
      this.tdLoadingService.resolve()
    });
  }
}
