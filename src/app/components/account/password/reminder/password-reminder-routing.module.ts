import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PasswordReminderComponent } from './password-reminder.component';

const routes: Routes = [
  {
    path: 'password', children: [
      { path: 'reminder', component: PasswordReminderComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class PasswordReminderRoutingModule { }
