import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { PasswordReminderComponent } from './password-reminder.component';
import { PasswordReminderRoutingModule } from './password-reminder-routing.module';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    PasswordReminderComponent
  ],
  imports: [
    PasswordReminderRoutingModule,

    APP_MODULES
  ],
  providers: []
})
export class PasswordReminderModule { }
