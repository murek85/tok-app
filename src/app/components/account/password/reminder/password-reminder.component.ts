import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

declare var $: any;

@Component({
  selector: 'app-password-reminder',
  templateUrl: './password-reminder2.component.html',
  styleUrls: ['./password-reminder2.component.scss']
})
export class PasswordReminderComponent implements OnInit, AfterViewInit {

  userName;
  password;
  email;
  success;
  error;

  returnUrl: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    public sidebarService: SidebarService,
    public accountService: AccountService) { }

  private initSemanticConfig() {
    $('.ui.form-password').form({
      fields: {
        email: {
          identifier: 'email',
          rules: [
            {
              type: 'email',
              prompt: `Pole '{name}' wymaga wprowadzenia poprawnego adresu e-mail.`
            }
          ]
        }
      }
    });
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    if (this.accountService.getLoggedIn()) {
      this.router.navigate(['/dashboard']);
    } else {
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
    }
  }

  public reminderPassword() {
    this.error = null; this.success = null;

    if (!$('.ui.form-password').form('is valid')) {
      return;
    }

    this.tdLoadingService.register();
    const model = { email: this.email }
    this.accountService.reminderPassword(model)
    .then(result => {
      // $('body').toast({
      //   title: 'Wiadomość wysłana',
      //   message: 'Pomyślnie wysłano wiadomość z linkiem resetującym hasło do konta użytkownika.',
      //   showProgress: 'bottom',
      //   progressUp: true
      // });
      this.success = {
        message: 'Pomyślnie wysłano wiadomość z linkiem resetującym hasło do konta użytkownika.'
      };
      console.log(result);
    })
    .catch(err => {
      console.error(err);
      this.error = err;
    })
    .finally(() => {
      this.tdLoadingService.resolve()
    });
  }
}
