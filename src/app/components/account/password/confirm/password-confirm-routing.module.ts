import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PasswordConfirmComponent } from './password-confirm.component';

const routes: Routes = [
  {
    path: 'password', children: [
      { path: 'confirm', component: PasswordConfirmComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class PasswordConfirmRoutingModule { }
