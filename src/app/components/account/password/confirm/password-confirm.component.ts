import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

declare var $: any;

@Component({
  selector: 'app-password-confirm',
  templateUrl: './password-confirm.component.html',
  styleUrls: ['./password-confirm.component.scss']
})
export class PasswordConfirmComponent implements OnInit, AfterViewInit {

  newPassword: string;
  repeatPassword: string;
  success;
  error;

  returnUrl: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    public sidebarService: SidebarService,
    public accountService: AccountService) { }

  private initSemanticConfig() {
    $('.ui.form-password').form({
      fields: {
        newPassword: {
          identifier: 'newPassword',
          rules: [
            {
              type: 'minLength[6]',
              prompt: `Pole '{name}' wymaga wprowadzenia minimum {ruleValue} znaków.`
            }
          ]
        },
        repeatPassword: {
          identifier: 'repeatPassword',
          rules: [
            {
              type: 'minLength[6]',
              prompt: `Pole '{name}' wymaga wprowadzenia minimum {ruleValue} znaków.`
            }
          ]
        }
      }
    });
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    if (this.accountService.getLoggedIn()) {
      this.router.navigate(['/dashboard']);
    } else {
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
    }
  }

  public confirmPassword() {
    this.error = null; this.success = null;

    if (!$('.ui.form-password').form('is valid')) {
      return;
    }

    this.tdLoadingService.register();
    const userId = this.route.snapshot.queryParams['userId'];
    const token = this.route.snapshot.queryParams['token'];
    const model = { newPassword: this.newPassword, repeatPassword: this.repeatPassword }
    this.accountService.confirmPassword(model, userId, token)
    .then(result => {
      // $('body').toast({
      //   title: 'Hasło zmienione',
      //   message: 'Twoje nowe hasło do konta użytkownika zostało pomyślnie zmienione.',
      //   showProgress: 'bottom',
      //   progressUp: true
      // });
      this.success = {
        message: 'Twoje nowe hasło do konta użytkownika zostało pomyślnie zmienione.'
      };
      console.log(result);
    })
    .catch(err => {
      console.error(err);
      this.error = err;
    })
    .finally(() => {
      this.tdLoadingService.resolve();
    });
  }
}
