import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { MatTabChangeEvent } from '@angular/material/tabs';

import { filter } from 'rxjs/operators';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { UserModel } from 'src/app/shared/models/user.model';

import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, AfterViewInit {

  userLogged: UserModel;

  system = { version: '1.0.0', date: '21-10-2019', type: 'dev', appName: 'Nazwa aplikacji' };
  languages: any[] = AppConfig.settings.i18n.availableLanguages;

  selectedIndex = 0;

  stats = [];
  notifications = [];

  headerConfig = {
    title: 'Moje konto',
    meta: 'Ustawienia konta użytkownika',
    icon: {
      name: '',
      color: ''
    }
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private sidebarService: SidebarService,
    public accountService: AccountService) { }

  private initSemanticConfig() {
    $('.tabs-profile .item').tab();

    $('.settings').dropdown({
      action: 'hide'
    });
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);

    this.sidebarService.headerConfig.next(this.headerConfig);
    this.sidebarService.headerVisible = true;
    this.sidebarService.menuVisible = false;

    this.stats = [
      {
        name: 'poprawne logowanie', icon: 'clock outline', type: 'datetime',
        value: `${moment(this.userLogged?.lastValidLogin).format('DD-MM-YYYY HH:mm:ss') }`,
        color: 'rgba(28,174,196,1)'
      },
      {
        name: 'nieudane logowanie', icon: 'clock outline', type: 'datetime',
        value: `${moment(this.userLogged?.lastIncorrectLogin).format('DD-MM-YYYY HH:mm:ss') }`,
        color: 'rgba(239,154,154,1)'
      }
    ];

    this.system = {
      version: AppConfig.settings.system.version,
      date: AppConfig.settings.system.date,
      type: AppConfig.settings.system.type,
      appName: AppConfig.settings.system.appName
    };

    this.route.params.pipe(filter(params => params.type))
      .subscribe(params => {
        switch (params.type) {
          case 'details':
            this.selectedIndex = 0;
            break;
          case 'security':
            this.selectedIndex = 1;
            break;
          case 'history':
            this.selectedIndex = 2;
            break;
        }
      }
    );
  }

  public changeTab($event) {
    const tab = $event as MatTabChangeEvent;
    switch (tab.index) {
      case 0:
        break;
      case 1:
        break;
      case 2:
        break;
      case 3:
        setTimeout(_ => {
          $('.checkbox-account-1').checkbox({
            onChecked: () => {
            },
            onUnchecked: () => {
            }
          });

          $('.checkbox-account-2').checkbox({
            onChecked: () => {
            },
            onUnchecked: () => {
            }
          });

          $('.checkbox-account-3').checkbox({
            onChecked: () => {
            },
            onUnchecked: () => {
            }
          });
        }, 0);
        break;
    }
  }

  public saveForm() {
    this.tdLoadingService.register('listSyntax');
    $('body').toast({
      title: 'Pomyślnie zapisano',
      message: 'Poprawnie zapisano nowe informacje dotyczące Twojego konta użytkownika.',
      showProgress: 'bottom',
      progressUp: true,
      onVisible: () => {
        this.tdLoadingService.resolve('listSyntax');
      }
    });
  }
}
