import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login2.component.html',
  styleUrls: ['./login2.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {

  userName;
  password;
  email;
  error;

  returnUrl: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    public sidebarService: SidebarService,
    public accountService: AccountService) { }

  private initSemanticConfig() {
    $('.ui.form-login').form({
      fields: {
        userName: {
          identifier: 'userName',
          rules: [
            {
              type: 'empty',
              prompt: `Pole '{name}' nie może być puste.`
            }
          ]
        },
        password: {
          identifier: 'password',
          rules: [
            {
              type: 'minLength[6]',
              prompt: `Pole '{name}' wymaga wprowadzenia minimum {ruleValue} znaków.`
            }
          ]
        }
      },
    });
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    if (this.accountService.getLoggedIn()) {
      this.router.navigate(['/dashboard']);
    } else {
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
    }
  }

  public loginPasswordFlow() {
    this.error = null;

    if (!$('.ui.form-login').form('is valid')) {
      return;
    }

    this.tdLoadingService.register();
    this.accountService.loginPasswordFlow(this.userName, this.password)
    .then(result => {
      this.router.navigateByUrl(this.returnUrl);
    })
    .catch(err => {
      console.error(err);
      this.error = err;
    }).finally(() => {
      this.tdLoadingService.resolve()
    });
  }

  public loginAuthorizationCode(providerKey) {
    this.accountService.loginAuthorizationCode(providerKey);
  }
}
