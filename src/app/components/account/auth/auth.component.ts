import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxMAuthOidcService } from 'src/app/core/services/oidc/oidc.service';
import { AccountService } from 'src/app/core/services/account.service';

@Component({
    selector: 'app-auth',
    template: '<span></span>'
})
export class AuthComponent implements OnInit {

  returnUrl: string;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private accountService: AccountService,
              private oidcService: NgxMAuthOidcService) { }

  public ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';

    this.oidcService.events.subscribe(e => {
      switch (e.type) {
        case 'token_received':
        case 'token_refreshed': {
          this.accountService.getUserLogged().then(_ => this.router.navigateByUrl(this.returnUrl));
          break;
        }
      }
    });

    if (this.accountService.getLoggedIn()) {
      this.router.navigate(['/dashboard']);
    }
  }
}
