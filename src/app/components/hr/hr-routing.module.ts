import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HrComponent } from './hr.component';

import { AuthGuard } from 'src/app/core/services/auth.guard';
import { PermissionsTypes } from 'src/app/shared/enums/permissions-types.enum';

const onlyPermissions = [
  PermissionsTypes.HR
];

const routes: Routes = [
  {
    path: 'hr',
    children: [
      {
        path: '',
        redirectTo: 'personal-data',
        pathMatch: 'full'
      },
      {
        path: ':type',
        component: HrComponent,
        data: {
          state: 'hr',
          permissions: { only: onlyPermissions, redirectTo: '/accessdenied' }
        },
        canActivate: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class HrRoutingModule { }
