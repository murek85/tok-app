import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { HrComponent } from './hr.component';
import { HrRoutingModule } from './hr-routing.module';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    HrComponent
  ],
  imports: [
    HrRoutingModule,

    APP_MODULES
  ],
  providers: []
})
export class HrModule { }
