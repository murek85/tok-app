import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { FinancesComponent } from './finances.component';
import { FinancesRoutingModule } from './finances-routing.module';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    FinancesComponent
  ],
  imports: [
    FinancesRoutingModule,

    APP_MODULES
  ],
  providers: []
})
export class FinancesModule { }
