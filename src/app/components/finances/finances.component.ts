import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { MatTabChangeEvent } from '@angular/material/tabs';

import { filter } from 'rxjs/operators';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { UserModel } from 'src/app/shared/models/user.model';

import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-finances',
  templateUrl: './finances.component.html',
  styleUrls: ['./finances.component.scss']
})
export class FinancesComponent implements OnInit, AfterViewInit {

  userLogged: UserModel;

  system = { version: '1.0.0', date: '21-10-2019', type: 'dev', appName: 'Nazwa aplikacji' };
  languages: any[] = AppConfig.settings.i18n.availableLanguages;

  selectedIndex = 0;

  notifications = [];

  headerConfig = {
    title: 'Finanse',
    meta: '',
    icon: {
      name: '',
      color: ''
    }
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private sidebarService: SidebarService,
    public accountService: AccountService) { }

  private initSemanticConfig() {
    $('.tabs-profile .item').tab();

    $('.settings').dropdown({
      action: 'hide'
    });
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);

    this.sidebarService.headerConfig.next(this.headerConfig);
    this.sidebarService.headerVisible = true;

    this.system = {
      version: AppConfig.settings.system.version,
      date: AppConfig.settings.system.date,
      type: AppConfig.settings.system.type,
      appName: AppConfig.settings.system.appName
    };

    this.route.params.pipe(filter(params => params.type))
      .subscribe(params => {
        switch (params.type) {
          case 'earnings':
            this.selectedIndex = 0;
            break;
          case 'bank-account':
            this.selectedIndex = 1;
            break;
          case 'taxes':
            this.selectedIndex = 2;
            break;
          case 'social-security':
            this.selectedIndex = 3;
            break;
          case 'organisations':
            this.selectedIndex = 4;
            break;
        }
      }
    );
  }

  public changeTab($event) {
    const tab = $event as MatTabChangeEvent;
    switch (tab.index) {
      case 0:
        break;
      case 1:
        break;
      case 2:
        break;
      case 3:
        break;
    }
  }
}
