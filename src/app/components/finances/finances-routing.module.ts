import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FinancesComponent } from './finances.component';

import { AuthGuard } from 'src/app/core/services/auth.guard';
import { PermissionsTypes } from 'src/app/shared/enums/permissions-types.enum';

const onlyPermissions = [
  PermissionsTypes.FINANCES
];

const routes: Routes = [
  {
    path: 'finances', children: [
      {
        path: '',
        redirectTo: 'earnings',
        pathMatch: 'full'
      },
      {
        path: ':type',
        component: FinancesComponent,
        data: {
          state: 'finances',
          permissions: { only: onlyPermissions, redirectTo: '/accessdenied' }
        },
        canActivate: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class FinancesRoutingModule { }
