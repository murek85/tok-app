import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountService } from 'src/app/core/services/account.service';
import { ErrorModel } from 'src/app/shared/models/error.model';

@Component({
    selector: 'app-error',
    templateUrl: './error.component.html',
    styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {

  error: ErrorModel;

  constructor(private router: Router,
    private route: ActivatedRoute) { }

  public ngOnInit(): void {
    this.error = {
      type: this.route.snapshot.queryParams['type'],
      message: this.route.snapshot.queryParams['message']
    };
  }
}
