import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { ErrorComponent } from './error.component';
import { ErrorRoutingModule } from './error-routing.module';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    ErrorComponent
  ],
  imports: [
    ErrorRoutingModule,

    APP_MODULES
  ],
  providers: []
})
export class ErrorModule { }
