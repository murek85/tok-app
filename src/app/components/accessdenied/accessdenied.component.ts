import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-accessdenied',
    templateUrl: './accessdenied.component.html',
    styleUrls: ['./accessdenied.component.scss']
})
export class AccessDeniedComponent implements OnInit {

  returnUrl: string;

  constructor(private router: Router,
    private route: ActivatedRoute) { }

  public ngOnInit(): void {
    sessionStorage.setItem('lock-account', JSON.stringify(true));
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
  }

  public return(): void {
    sessionStorage.setItem('lock-account', JSON.stringify(false));
    this.router.navigateByUrl(this.returnUrl);
  }
}
