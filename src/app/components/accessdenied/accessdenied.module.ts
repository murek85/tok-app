import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { AccessDeniedComponent } from './accessdenied.component';
import { AccessDeniedRoutingModule } from './accessdenied-routing.module';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    AccessDeniedComponent
  ],
  imports: [
    AccessDeniedRoutingModule,

    APP_MODULES
  ],
  providers: []
})
export class AccessDeniedModule { }
