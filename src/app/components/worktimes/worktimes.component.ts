import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { MatTabChangeEvent } from '@angular/material/tabs';

import { filter } from 'rxjs/operators';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { UserModel } from 'src/app/shared/models/user.model';

import { DashboardWidget } from 'src/app/shared/models/dashboard-widget.model';
import { DashboardConfig } from 'src/app/shared/models/dashboard-config.model';
import { DashboardType } from 'src/app/shared/enums/dashboard-type.enum';
import { WidgetType } from 'src/app/shared/enums/widget-type.enum';

import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-worktimes',
  templateUrl: './worktimes.component.html',
  styleUrls: ['./worktimes.component.scss']
})
export class WorktimesComponent implements OnInit, AfterViewInit {

  userLogged: UserModel;

  system = { version: '1.0.0', date: '21-10-2019', type: 'dev', appName: 'Nazwa aplikacji' };
  languages: any[] = AppConfig.settings.i18n.availableLanguages;

  selectedIndex = 0;

  notifications = [];

  headerConfig = {
    title: 'Urlopy i czasy pracy',
    meta: '',
    icon: {
      name: '',
      color: ''
    }
  };

  configHolidays: DashboardConfig = {
    type: DashboardType.Holidays,
    title: 'Urlopy i czas pracy'
  };
  widgetsHolidays: Array<DashboardWidget> = [
    {
      position: { top: 1, left: 1, height: 49, width: 60 },
      settings: { type: WidgetType.MyVacation, header: 'Moje wnioski urlopowe', icon: '', theme: 'none' },
      active: true
    },
    {
      position: { top: 1, left: 61, height: 49, width: 60 },
      settings: { type: WidgetType.MySubstitutes, header: 'Moje zastępstwa', icon: '', theme: 'none'},
      active: true
    }
  ];

  configAbsences: DashboardConfig = {
    type: DashboardType.Absences,
    title: 'Wykaz absencji'
  };
  widgetsAbsences: Array<DashboardWidget> = [
    {
      position: { top: 1, left: 1, height: 80, width: 60 },
      settings: { type: WidgetType.MyAbsences, header: 'Wykaz absencji', icon: '', theme: 'none' },
      active: true
    }
  ];

  configHolidayPlan: DashboardConfig = {
    type: DashboardType.HolidayPlan,
    title: 'Plan urlopu'
  };
  widgetsHolidayPlan: Array<DashboardWidget> = [
    {
      position: { top: 1, left: 1, height: 80, width: 60 },
      settings: { type: WidgetType.MyHolidayPlan, header: 'Plan urlopu', icon: '', theme: 'none' },
      active: true
    },
    {
      position: { top: 1, left: 1, height: 80, width: 60 },
      settings: { type: WidgetType.MySubstitutes2, header: 'Moje zastępstwa', icon: '', theme: 'none' },
      active: true
    },
    {
      position: { top: 1, left: 1, height: 80, width: 60 },
      settings: { type: WidgetType.MyPlanAndRealization, header: 'Plan i realizacja', icon: '', theme: 'none' },
      active: true
    }
  ];

  configWorktime: DashboardConfig = {
    type: DashboardType.WorkTime,
    title: 'Nadgodziny'
  };
  widgetsWorktime: Array<DashboardWidget> = [
    {
      position: { top: 1, left: 1, height: 80, width: 60 },
      settings: { type: WidgetType.MyWorktime, header: 'Nadgodziny do wybrania', icon: '', theme: 'none' },
      active: true
    }
  ];

  configSchedules: DashboardConfig = {
    type: DashboardType.Schedules,
    title: 'Harmonogram'
  };
  widgetsSchedules: Array<DashboardWidget> = [
    {
      position: { top: 1, left: 1, height: 80, width: 60 },
      settings: { type: WidgetType.MySchedules, header: 'Harmonogram', icon: '', theme: 'none' },
      active: true
    }
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private sidebarService: SidebarService,
    public accountService: AccountService) { }

  private initSemanticConfig() {
    $('.tabs-profile .item').tab();

    $('.settings').dropdown({
      action: 'hide'
    });
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);

    this.sidebarService.headerConfig.next(this.headerConfig);
    this.sidebarService.headerVisible = true;

    this.system = {
      version: AppConfig.settings.system.version,
      date: AppConfig.settings.system.date,
      type: AppConfig.settings.system.type,
      appName: AppConfig.settings.system.appName
    };

    this.route.params.pipe(filter(params => params.type))
      .subscribe(params => {
        switch (params.type) {
          case 'holidays':
            this.selectedIndex = 0;
            break;
          case 'absences':
            this.selectedIndex = 1;
            break;
          case 'holiday-plan':
            this.selectedIndex = 2;
            break;
          case 'worktime':
            this.selectedIndex = 3;
            break;
          case 'schedules':
            this.selectedIndex = 4;
            break;
        }
      }
    );
  }

  public changeTab($event) {
    const tab = $event as MatTabChangeEvent;
    switch (tab.index) {
      case 0:
        break;
      case 1:
        break;
      case 2:
        break;
      case 3:
        break;
    }
  }
}
