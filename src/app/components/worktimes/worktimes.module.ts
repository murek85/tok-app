import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { WorktimesComponent } from './worktimes.component';
import { WorktimesRoutingModule } from './worktimes-routing.module';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    WorktimesComponent
  ],
  imports: [
    WorktimesRoutingModule,

    APP_MODULES
  ],
  providers: []
})
export class WorktimesModule { }
