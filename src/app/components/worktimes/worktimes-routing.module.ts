import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorktimesComponent } from './worktimes.component';

import { AuthGuard } from 'src/app/core/services/auth.guard';
import { PermissionsTypes } from 'src/app/shared/enums/permissions-types.enum';

const onlyPermissions = [
  PermissionsTypes.WORKTIMES
];

const routes: Routes = [
  {
    path: 'worktimes', children: [
      {
        path: '',
        redirectTo: 'holidays',
        pathMatch: 'full'
      },
      {
        path: ':type',
        component: WorktimesComponent,
        data: {
          state: 'worktimes',
          permissions: { only: onlyPermissions, redirectTo: '/accessdenied' }
        },
        canActivate: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class WorktimesRoutingModule { }
