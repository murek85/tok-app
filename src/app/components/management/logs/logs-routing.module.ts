import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LogsComponent } from './logs.component';

import { AuthGuard } from 'src/app/core/services/auth.guard';
import { PermissionsTypes } from 'src/app/shared/enums/permissions-types.enum';

const onlyPermissions = [
  PermissionsTypes.LOGS
];

const routes: Routes = [
  {
    path: 'management',
    children: [
      {
        path: 'logs',
        component: LogsComponent,
        data: {
          state: 'logs',
          permissions: { only: onlyPermissions, redirectTo: '/accessdenied' }
        },
        canActivate: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class LogsRoutingModule { }
