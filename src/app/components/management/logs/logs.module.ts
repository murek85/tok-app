import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { LogsComponent } from './logs.component';
import { LogsRoutingModule } from './logs-routing.module';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    LogsComponent
  ],
  imports: [
    LogsRoutingModule,

    APP_MODULES
  ],
  providers: []
})
export class LogsModule { }
