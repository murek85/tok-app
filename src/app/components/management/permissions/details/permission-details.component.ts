import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ViewChild, ElementRef, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { finalize } from 'rxjs/operators';

import { AppConfig } from 'src/app/app.config';

import { PermissionsService } from '../permissions.service';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { UserModel } from 'src/app/shared/models/user.model';
import { PermissionModel } from 'src/app/shared/models/permission.model';
import { RoleModel } from 'src/app/shared/models/role.model';
import { ClaimModel } from 'src/app/shared/models/claim.model';

import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-permission-details',
  templateUrl: './permission-details.component.html',
  styleUrls: ['./permission-details.component.scss']
})
export class PermissionDetailsComponent implements OnInit, AfterViewInit {

  userLogged: UserModel;

  permission: PermissionModel;

  role: RoleModel;
  claims: ClaimModel[];

  permissionId = null;

  headerConfig = {
    title: 'Uprawnienia',
    meta: 'Uprawnienie',
    icon: {
      name: 'key',
      color: ''
    }
  };

  error: any;

  selectedIndex = 0;

  menus = [
      { label: 'Szczegóły konta', routerLink: '/management/users/' },
      { label: 'Bezpieczeństwo', routerLink: '/management/users/' }
  ];

  activeMenu = null;

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private accountService: AccountService,
    private permissionsService: PermissionsService,
    private sidebarService: SidebarService) {

    this.permissionId = this.route.snapshot.params['id'];

    this.menus.forEach(menu => menu.routerLink += `${this.permissionId}`); this.activeMenu = this.menus[0];

    this.claims = [];
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);
    this.accountService.getUserLogged();

    this.sidebarService.headerConfig.next(this.headerConfig);
    this.sidebarService.headerVisible = true;
    this.sidebarService.menuVisible = false;

    this.permissionsService.permission.subscribe(permission => this.permission = permission);
    this.permissionsService.getPermission(this.permissionId);

    this.initClaims();
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public changeTab($event) {
    const tab = $event as MatTabChangeEvent;
    switch (tab.index) {
      case 0:
        break;
      case 1:
        break;
    }
  }

  private initSemanticConfig() {
    $('.tabs-profile .item').tab();

    $('.account-settings').dropdown({
      action: 'hide'
    });
  }

  private initSemanticClaims() {
    setTimeout(_ => {
      $('.dropdown-claims').dropdown({ });
    }, 0);
  }

  private initClaims() {
    const claims: ClaimModel[] = [];
    claims.push({
      name: 'Zarządzanie kontami',
      value: 'ZARZADZANIE_KONTAMI',
      application: { name: 'epr', value: 'EPR' }
    });
    claims.push({
      name: 'Zarządzanie uprawnieniami',
      value: 'ZARZADZANIE_UPRAWNIENIAMI',
      application: { name: 'epr', value: 'EPR' }
    });
    claims.push({
      name: 'Raporty statystyczne',
      value: 'RAPORTY_STATYSTYCZNE',
      application: { name: 'epr', value: 'EPR' }
    });

    this.claims = claims;
  }

  public saveForm() {
  }
}
