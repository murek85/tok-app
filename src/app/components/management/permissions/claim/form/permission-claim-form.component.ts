import { Component, Input, OnInit, AfterViewInit, ApplicationInitStatus } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CdkStepper } from '@angular/cdk/stepper';
import { TranslateService } from '@ngx-translate/core';
import { TdLoadingService } from '@covalent/core/loading';
import { iif } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { AccountService } from 'src/app/core/services/account.service';
import { PermissionsService } from '../../permissions.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { ApplicationModel } from 'src/app/shared/models/application.model';
import { ClaimModel } from 'src/app/shared/models/claim.model';
import { StepModel, SegmentModel, FormFieldModel } from 'src/app/shared/models/step.model';

import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';

import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-permission-claim-form',
  styleUrls: ['./permission-claim-form.component.scss'],
  templateUrl: './permission-claim-form.component.html'
})
export class PermissionClaimFormComponent implements OnInit, AfterViewInit {

  applications: ApplicationModel[]
  steps: StepModel[];

  headerConfig = {
    title: 'Uprawnienia',
    meta: 'Nowe uprawnienie',
    symbol: 'permission',
    icon: {
      name: 'key',
      color: ''
    }
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private translateService: TranslateService,
    private tdLoadingService: TdLoadingService,
    private accountService: AccountService,
    private permissionsService: PermissionsService,
    private sidebarService: SidebarService) {

    this.applications = [];
    this.steps = [];
  }

  public ngOnInit() {
    this.initApplications();
		this.initSteps();
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
    this.initSemanticForm();
  }

  public onChangeStep($event) {
    const stepper = $event as CdkStepper;
    switch (this.steps[stepper.selectedIndex].symbol) {
      case 'permission-app': {
        this.initForm();
        break;
      }
      case 'permission-data': {
        this.initForm();
        break;
      }
    }
  }

  public onPreviousStep($event) {
  }

  public onNextStep($event) {
  }

  public onSaveForm(model: ClaimModel) {
    $('body').toast({
      title: 'Pomyślnie zapisano',
      message: `Poprawnie utworzono nowe uprawnienie o nazwie '${model.name}'.`,
      showProgress: 'bottom',
      progressUp: true,
      onVisible: () => {
        this.router.navigate(['/management/permissions/list']); console.log(model);
      }
    });
  }

  public onBackForm($event) {
    this.router.navigate(['/management/permissions/list']);
  }

  private initSemanticForm() {
    this.initForm();
  }

  private initForm() {
    setTimeout(_ => {
      $(`.ui.form-${this.headerConfig.symbol}`).form({
        fields: {
          application: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          },
          name: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          },
          code: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          },
          description: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          }
        },
        inline : false
      });
    }, 0);
  }

  private initSemanticConfig() {
  }

  private initApplications() {
    const applications: ApplicationModel[] = [];
    applications.push({ name: 'epr', value: 'EPR' });

    this.applications = applications;
  }

  private initSteps() {
    const steps: StepModel[] = [];

    steps.push(this.createStep(
      'Aplikacja',
      'Wybranie aplikacji w ramach której utworzona zostanie nowe uprawnienie.',
      'permission-app',
      [
        {
          header: 'Aplikacja',
					description: 'Wybranie aplikacji w systemie w ramach której utworzone będzie uprawnienie.',
          fields: [
            {
              label: 'Aplikacja',
              name: 'application',
              symbol: 'permission-application',
              type: FormFieldType.Single,
              items: this.applications,
              value: null
            }
          ]
        }
      ]
    ));

    steps.push(this.createStep(
      'Uprawnienie',
      'Wprowadzenie podstawowych informacji o nowym uprawnieniu w aplikacji.',
      'permission-data',
      [
        {
          header: 'Uprawnienie',
          description: 'Podstawowe informacje o tworzonym uprawnieniu w aplikacji.',
          fields: [
            {
							label: 'Nazwa uprawnienia',
							name: 'name',
							symbol: 'permission-name',
              type: FormFieldType.Text,
              value: null
						},
            {
							label: 'Kod uprawnienia',
							name: 'code',
							symbol: 'permission-code',
              type: FormFieldType.Text,
							hints: [
								'Specjalny kod uprawnienia pozwalający użytkownikowi na dostęp do odpowiednich zasobów w aplikacji.'
							],
              value: null
						},
            {
							label: 'Opis uprawnienia',
							name: 'description',
							symbol: 'permission-description',
              type: FormFieldType.Text,
              value: null
						},
          ]
        }
      ]
    ));

    this.steps = steps;
  }

  private createStep(
    label: string, description: string, symbol: string, segments: SegmentModel[], completed: boolean = false): StepModel {

    const menu: StepModel = { label, description, symbol, segments, completed };
    return menu;
  }
}
