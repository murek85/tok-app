import { Component, Input, OnInit, AfterViewInit, ApplicationInitStatus } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CdkStepper } from '@angular/cdk/stepper';
import { TranslateService } from '@ngx-translate/core';
import { TdLoadingService } from '@covalent/core/loading';
import { iif } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { AccountService } from 'src/app/core/services/account.service';
import { PermissionsService } from '../../permissions.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { ApplicationModel } from 'src/app/shared/models/application.model';
import { ClaimModel } from 'src/app/shared/models/claim.model';
import { StepModel, SegmentModel, FormFieldModel } from 'src/app/shared/models/step.model';

import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';

import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-permission-role-form',
  styleUrls: ['./permission-role-form.component.scss'],
  templateUrl: './permission-role-form.component.html'
})
export class PermissionRoleFormComponent implements OnInit, AfterViewInit {

  applications: ApplicationModel[]
  claims: ClaimModel[];
  steps: StepModel[];

  headerConfig = {
    title: 'Role',
    meta: 'Nowa rola',
    symbol: 'role',
    icon: {
      name: 'key',
      color: ''
    }
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private translateService: TranslateService,
    private tdLoadingService: TdLoadingService,
    private accountService: AccountService,
    private permissionsService: PermissionsService,
    private sidebarService: SidebarService) {

    this.applications = [];
    this.claims = [];
    this.steps = [];
  }

  public ngOnInit() {
    this.initApplications();
    this.initClaims();
    this.initSteps();
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
    this.initSemanticForm();
  }

  public onChangeStep($event) {
    const stepper = $event as CdkStepper;
    switch (this.steps[stepper.selectedIndex].symbol) {
      case 'role-app': {
        this.initForm();
        break;
      }
      case 'role-data': {
        this.initForm();
        break;
      }
      case 'role-claim': {
        this.initForm();
        break;
      }
    }
  }

  public onPreviousStep($event) {
  }

  public onNextStep($event) {
  }

  public onSaveForm(model: ClaimModel) {
    $('body').toast({
      title: 'Pomyślnie zapisano',
      message: `Poprawnie utworzono nowe uprawnienie o nazwie '${model.name}'.`,
      showProgress: 'bottom',
      progressUp: true,
      onVisible: () => {
        // this.router.navigate(['/management/permissions/list']); console.log(model);
      }
    });
  }

  public onBackForm($event) {
    this.router.navigate(['/management/permissions/list']);
  }

  private initSemanticForm() {
    this.initForm();
  }

  private initForm() {
    setTimeout(_ => {
      $(`.ui.form-${this.headerConfig.symbol}`).form({
        fields: {
          application: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          },
          name: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          },
          code: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          },
          description: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          },
          claim: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              },
              // {
              //   type: 'minCount[1]',
              //   prompt: `Pole '{name}' wymaga wybrania conajmniej {ruleValue} wartości.`
              // }
            ]
          },
        },
        inline : false
      });
    }, 0);
  }

  private initSemanticConfig() {
  }

  private initApplications() {
    const applications: ApplicationModel[] = [];
    applications.push({ name: 'epr', value: 'EPR' });

    this.applications = applications;
  }

  private initClaims() {
    const claims: ClaimModel[] = [];
    claims.push({
      name: 'Zarządzanie użytkownikami',
      value: 'ZARZADZANIE_UZYTKOWNIKAMI',
      // application: { name: 'epr', value: 'EPR' }
    });
    claims.push({
      name: 'Zarządzanie uprawnieniami',
      value: 'ZARZADZANIE_UPRAWNIENIAMI',
      // application: { name: 'epr', value: 'EPR' }
    });
    claims.push({
      name: 'Raporty statystyczne',
      value: 'RAPORTY_STATYSTYCZNE',
      // application: { name: 'epr', value: 'EPR' }
    });

    this.claims = claims;
  }

  private initSteps() {
    const steps: StepModel[] = [];

    steps.push(this.createStep(
      'Aplikacja',
      'Wybranie aplikacji w ramach której utworzona zostanie nowa rola.',
      'role-app',
      [
        {
          header: 'Aplikacja',
					description: 'Wybranie aplikacji w systemie w ramach której utworzona będzie rola.',
          fields: [
            {
              label: 'Aplikacja',
              name: 'application',
              symbol: 'role-application',
              type: FormFieldType.Single,
              items: this.applications,
              value: null
            }
          ]
        }
      ]
    ));

    steps.push(this.createStep(
      'Rola',
      'Wprowadzenie podstawowych informacji o nowej roli w aplikacji.',
      'role-data',
      [
        {
          header: 'Rola',
          description: 'Podstawowe informacje o tworzonej roli w aplikacji.',
          fields: [
            {
							label: 'Nazwa roli',
							name: 'name',
							symbol: 'role-name',
              type: FormFieldType.Text,
              value: null
						},
            {
							label: 'Kod roli',
							name: 'code',
							symbol: 'role-code',
              type: FormFieldType.Text,
							hints: [
								'Specjalny kod roli pozwalający użytkownikowi na dostęp do odpowiednich modułów w aplikacji.'
							],
              value: null
						},
            {
							label: 'Opis roli',
							name: 'description',
							symbol: 'role-description',
              type: FormFieldType.Text,
              value: null
						},
          ]
        }
      ]
    ));

    steps.push(this.createStep(
      'Uprawnienia',
      'Przydzielenie wybranych uprawnień do roli, dostępnych w ramach danej aplikacji.',
      'role-claim',
      [
        {
          header: 'Uprawnienia',
          description: 'Wybranie uprawnień dla roli.',
          fields: [
            {
              label: 'Uprawnienia',
              name: 'claim',
              symbol: 'role-claim',
              type: FormFieldType.Multi,
              items: this.claims,
              value: null
            }
          ]
        }
      ]
    ));

    this.steps = steps;
  }

  private createStep(
    label: string, description: string, symbol: string, segments: SegmentModel[], completed: boolean = false): StepModel {

    const menu: StepModel = { label, description, symbol, segments, completed };
    return menu;
  }
}
