import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PermissionsComponent } from './permissions.component';
import { PermissionDetailsComponent } from './details/permission-details.component';
import { PermissionClaimFormComponent } from './claim/form/permission-claim-form.component';
import { PermissionRoleFormComponent } from './role/form/permission-role-form.component';

import { AuthGuard } from 'src/app/core/services/auth.guard';
import { PermissionsTypes } from 'src/app/shared/enums/permissions-types.enum';

const onlyPermissions = [
  PermissionsTypes.PERMISSIONS
];

const routes: Routes = [
  {
    path: 'management',
    children: [
      {
        path: 'permissions',
        children: [
          {
            path: '',
            redirectTo: 'list',
            pathMatch: 'full'
          },
          {
            path: 'list', component: PermissionsComponent,
            data: {
              state: 'permissions',
              permissions: { only: onlyPermissions, redirectTo: '/accessdenied' }
            },
            canActivate: [AuthGuard]
          },
          {
            path: 'claim',
            children: [
              {
                path: 'form',
                children: [
                  {
                    path: '',
                    component: PermissionClaimFormComponent,
                    canActivate: [AuthGuard]
                  },
                  {
                    path: ':id',
                    component: PermissionClaimFormComponent,
                    canActivate: [AuthGuard]
                  }
                ]
              },
            ]
          },
          {
            path: 'role',
            children: [
            {
              path: 'form',
              children: [
                {
                  path: '',
                  component: PermissionRoleFormComponent,
                  canActivate: [AuthGuard]
                },
                {
                  path: ':id',
                  component: PermissionRoleFormComponent,
                  canActivate: [AuthGuard]
                }
                ]
              },
            ]
          },
          {
            path: ':id',
            component: PermissionDetailsComponent,
            canActivate: [AuthGuard]
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PermissionsRoutingModule { }
