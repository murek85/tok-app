import { Component, OnInit, Input, Output, AfterViewInit } from '@angular/core';

import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { FilterObjectModel } from 'src/app/shared/models/filter.model';
import { FilterUserType } from 'src/app/shared/enums/filters/filter-user-type.enum';
import { FilterUserModel } from 'src/app/shared/models/filters/filter-user.model';

declare var $: any;

@Component({
  selector: 'app-permissions-filters',
  styleUrls: ['./permissions-filters.component.scss'],
  templateUrl: './permissions-filters.component.html'
})
export class PermissionsFiltersComponent implements OnInit, AfterViewInit {

  @Input() @Output() filters: FilterObjectModel<FilterUserType>[];

  filterUserType: typeof FilterUserType = FilterUserType;

  constructor(
    private loadingService: TdLoadingService,
    private translateService: TranslateService) {

    this.filters = [];
  }

  public ngOnInit() { }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  private initFilters() {
    this.filters.forEach(filter => {
      switch (filter.type) {

      }
    });
  }

  private initSemanticConfig() {
  }

  private findIndexWithFilters(value, filterType): number {
    const idx = this.filters.findIndex(filter => filter.value === (value) && filter.type === filterType);
    return idx;
  }

  private findIndexWithoutValue(filterType): number {
    const idx = this.filters.findIndex(filter => filter.type === filterType);
    return idx;
  }
}
