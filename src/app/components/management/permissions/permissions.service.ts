import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { PermissionsHttpService } from 'src/app/core/services/http/permissions.service';

import { TdLoadingService } from '@covalent/core/loading';

import { PagingSettingsModel, PagingResultsModel } from 'src/app/shared/models/paging.model';
import { FilterPermissionModel } from 'src/app/shared/models/filters/filter-permission.model';
import { PermissionModel } from 'src/app/shared/models/permission.model';
import { ResultModel } from 'src/app/shared/models/result.model';
import { ClaimModel } from 'src/app/shared/models/claim.model';
import { RoleModel } from 'src/app/shared/models/role.model';

@Injectable()
export class PermissionsService {

  private permissionsSource = new BehaviorSubject<PermissionModel[]>([]);
  private permissionSource = new BehaviorSubject<PermissionModel>(null);
  private filtersSource = new BehaviorSubject<FilterPermissionModel>(null);
  private pagingResultsSource = new BehaviorSubject<PagingResultsModel>(new PagingResultsModel());

  permissions = this.permissionsSource.asObservable();
  permission = this.permissionSource.asObservable();
  filters = this.filtersSource.asObservable();
  pagingResults = this.pagingResultsSource.asObservable();

  changePermissions(data: PermissionModel[]) {
    this.permissionsSource.next(data);
  }

  changePermission(data: any) {
      this.permissionSource.next(data);
  }

  changeFilters(data: FilterPermissionModel) {
      this.filtersSource.next(data);
  }

  changePagingResults(data: PagingResultsModel) {
      this.pagingResultsSource.next(data);
  }

  constructor(
    private tdLoadingService: TdLoadingService,
    private permissionsHttpService: PermissionsHttpService) { }

  public getPermissions(pagingSettings: PagingSettingsModel, filters: FilterPermissionModel) {
    this.tdLoadingService.register('listSyntax');
    this.permissionsHttpService.getPermissions(pagingSettings, filters).subscribe(
      (result: ResultModel<PermissionModel[]>) => {
        this.changeFilters(filters);
        this.changePagingResults(result.paging);
        this.changePermissions(result.data);
      },
      err => this.tdLoadingService.resolve('listSyntax'),
      () => this.tdLoadingService.resolve('listSyntax')
    );
  }

  public getPermission(permissionId) {
    this.tdLoadingService.register('listSyntax');
    this.permissionsHttpService.getPermission(permissionId).subscribe(
      user => this.changePermission(user),
      err => this.tdLoadingService.resolve('listSyntax'),
      () => this.tdLoadingService.resolve('listSyntax')
    );
  }

  public createRole(role: RoleModel) {
    return this.permissionsHttpService.createRole(role);
  }

  public updateRole(role: RoleModel) {
    return this.permissionsHttpService.updateRole(role);
  }

  public deleteRole(roleId) {
    return this.permissionsHttpService.deleteRole(roleId);
  }

  public createClaim(claim: ClaimModel) {
    return this.permissionsHttpService.createClaim(claim);
  }
}
