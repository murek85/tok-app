import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { PermissionsService } from './permissions.service';

import { PermissionsComponent } from './permissions.component';
import { PermissionsRoutingModule } from './permissions-routing.module';

import { PermissionDetailsComponent } from './details/permission-details.component';
import { PermissionClaimFormComponent } from './claim/form/permission-claim-form.component';
import { PermissionRoleFormComponent } from './role/form/permission-role-form.component';

import { PermissionsFiltersComponent } from './filters/permissions-filters.component';

import { PermissionListComponent } from './list/permission-list.component';
import { PermissionTableComponent } from './table/permission-table.component';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    PermissionsComponent,
    PermissionsFiltersComponent,

    PermissionDetailsComponent,
    PermissionClaimFormComponent,
    PermissionRoleFormComponent,

    PermissionListComponent,
    PermissionTableComponent
  ],
  imports: [
    PermissionsRoutingModule,

    APP_MODULES
  ],
  entryComponents: [],
  providers: [
    PermissionsService
  ]
})
export class PermissionsModule { }
