import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ViewChildren, QueryList, HostListener, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { TdLoadingService } from '@covalent/core/loading';
import { IPageChangeEvent } from '@covalent/core/paging';
import { TranslateService } from '@ngx-translate/core';

import { isNullOrUndefined } from 'util';

import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/scrolling';

import { AppConfig } from 'src/app/app.config';

import { PermissionsService } from './permissions.service';

import { FilterObjectModel } from 'src/app/shared/models/filter.model';
import { SortObjectModel } from 'src/app/shared/models/sort.model';
import { PagingSettingsModel, PagingResultsModel } from 'src/app/shared/models/paging.model';
import { TableSettingsModel } from 'src/app/shared/models/table.model';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { UserModel } from 'src/app/shared/models/user.model';
import { PermissionModel } from 'src/app/shared/models/permission.model';

import { TableType } from 'src/app/shared/enums/table-type.enum';
import { FilterPermissionType } from 'src/app/shared/enums/filters/filter-permission-type.enum';
import { FilterPermissionModel } from 'src/app/shared/models/filters/filter-permission.model';

import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss']
})
export class PermissionsComponent implements OnInit, AfterViewInit {

  config: any = {
    type: 'permissions',
    header: {
      title: 'Uprawnienia',
      meta: 'Zarządzanie uprawnieniami',
      icon: {
        name: 'key',
        color: ''
      }
    }
  };

  userLogged: UserModel;

  data: PermissionModel[];
  currentItem: PermissionModel;

  pagingResults: PagingResultsModel = new PagingResultsModel();

  filterPemrissionType: typeof FilterPermissionType = FilterPermissionType;

  constructor(
    public dialog: MatDialog,
    private scrollDispatcher: ScrollDispatcher,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private permissionsService: PermissionsService,
    public accountService: AccountService,
    public sidebarService: SidebarService) {

    this.data = [];
  }

  private initSemanticConfig() { }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);
    this.accountService.getUserLogged();

    this.sidebarService.headerConfig.next(this.config.header);
    this.sidebarService.headerVisible = false;
    this.sidebarService.subheaderVisible = false;
    this.sidebarService.menuVisible = false;

    this.permissionsService.pagingResults.subscribe(pagingResults => this.pagingResults = pagingResults);

    this.permissionsService.permissions.subscribe(permissions => {
      this.data = permissions; this.currentItem = permissions[0]; this.tdLoadingService.resolve('listSyntax');
    });
  }

  onRefreshData(params): void {
    const { pagingSettings, filters, sorts } = params;

    const filtersPermission: FilterPermissionModel = new FilterPermissionModel();

    this.permissionsService.getPermissions(pagingSettings, filtersPermission);
  }

  onAcceptFilter(event: any) {
  }

  onClearFilter(event: any) {
  }

  onRemoveFilter(event: any) {
    switch (event.type) {
      default:
        break;
    }
  }
}
