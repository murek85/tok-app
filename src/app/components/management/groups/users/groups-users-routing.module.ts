import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GroupsUsersComponent } from './groups-users.component';
import { GroupUserDetailsComponent } from './details/group-user-details.component';
import { GroupUserFormComponent } from './form/group-user-form.component';

import { AuthGuard } from 'src/app/core/services/auth.guard';
import { PermissionsTypes } from 'src/app/shared/enums/permissions-types.enum';

const onlyPermissions = [
  PermissionsTypes.GROUPS
];

const routes: Routes = [
  {
    path: 'management',
    children: [
      {
        path: 'groups/users',
        children: [
          {
            path: '',
            redirectTo: 'list',
            pathMatch: 'full'
          },
          {
            path: 'list',
            component: GroupsUsersComponent,
            data: {
              state: 'groups',
              permissions: { only: onlyPermissions, redirectTo: '/accessdenied' }
            },
            canActivate: [AuthGuard]
          },
          {
            path: 'form',
            children: [
              {
                path: '',
                component: GroupUserFormComponent,
                canActivate: [AuthGuard]
              },
              {
                path: ':id',
                component: GroupUserFormComponent,
                canActivate: [AuthGuard]
              }
            ]
          },
          {
            path: ':id',
            component: GroupUserDetailsComponent,
            canActivate: [AuthGuard]
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class GroupsUsersRoutingModule { }
