import { Component, Input, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CdkStepper } from '@angular/cdk/stepper';
import { TranslateService } from '@ngx-translate/core';
import { TdLoadingService } from '@covalent/core/loading';
import { iif } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { AccountService } from 'src/app/core/services/account.service';
import { GroupsUsersService } from '../groups-users.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { ApplicationModel } from 'src/app/shared/models/application.model';
import { GroupModel } from 'src/app/shared/models/group.model';
import { StepModel, SegmentModel, FormFieldModel } from 'src/app/shared/models/step.model';

import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';

import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-group-user-form',
  styleUrls: ['./group-user-form.component.scss'],
  templateUrl: './group-user-form.component.html'
})
export class GroupUserFormComponent implements OnInit, AfterViewInit {

  applications: ApplicationModel[];
  steps: StepModel[];

  headerConfig = {
    title: 'Grupy użytkowników',
    meta: 'Nowa grupa użytkowników',
    symbol: 'group-users',
    icon: {
      name: 'plus',
      color: ''
    }
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private translateService: TranslateService,
    private tdLoadingService: TdLoadingService,
    private accountService: AccountService,
    private groupsUsersService: GroupsUsersService,
    private sidebarService: SidebarService) {

    this.applications = [];
    this.steps = [];
  }

  public ngOnInit() {
    this.initApplications();
		this.initSteps();
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
    this.initSemanticForm();
  }

  public onChangeStep($event) {
    const stepper = $event as CdkStepper;
    switch (this.steps[stepper.selectedIndex].symbol) {
      case 'group-app': {
        this.initForm();
        break;
      }
      case 'group-data': {
        this.initForm();
        break;
      }
    }
  }

  public onPreviousStep($event) {
  }

  public onNextStep($event) {
  }

  public onSaveForm(model: GroupModel) {
    $('body').toast({
      title: 'Pomyślnie zapisano',
      message: `Poprawnie utworzono nową grupę użytkowników o nazwie '${model.name}'.`,
      showProgress: 'bottom',
      progressUp: true,
      onVisible: () => {
        this.router.navigate(['/management/groups/users/list']); console.log(model);
      }
    });
  }

  public onBackForm($event) {
    this.router.navigate(['/management/groups/users/list']);
  }

  private initSemanticForm() {
    this.initForm();
  }

  private initForm() {
    setTimeout(_ => {
      $(`.ui.form-${this.headerConfig.symbol}`).form({
        fields: {
          application: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          },
          name: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          },
          code: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          },
          description: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          }
        },
        inline : false
      });
    }, 0);
  }

  private initSemanticConfig() {
  }

  private initApplications() {
    const applications: ApplicationModel[] = [];
    applications.push({ name: 'epr', value: 'EPR' });

    this.applications = applications;
  }

  private initSteps() {
    const steps: StepModel[] = [];

    steps.push(this.createStep(
      'Aplikacja',
      'Wybranie aplikacji w ramach której utworzona zostanie nowa grupa zasobów.',
      'group-app',
			[
				{
					header: 'Aplikacja',
					description: 'Wybranie aplikacji w systemie w ramach której utworzone będzie uprawnienie.',
					fields: [
						{
							label: 'Aplikacja',
							name: 'application',
							symbol: 'group-application',
							type: FormFieldType.Single,
              items: this.applications,
              value: null
						},
					]
				}
			]
		));

    steps.push(this.createStep(
      'Grupa',
      'Wprowadzenie podstawowych informacji o nowej grupie w aplikacji.',
      'group-data',
			[
				{
					header: 'Grupa',
					description: 'Podstawowe informacje o tworzonej grupie w aplikacji',
					fields: [
						{
							label: 'Nazwa grupy',
							name: 'name',
							symbol: 'group-name',
              type: FormFieldType.Text,
              value: null
						},
						{
							label: 'Kod grupy',
							name: 'code',
							symbol: 'group-code',
							type: FormFieldType.Text,
							hints: [
								'Specjalny kod grupy pozwalający użytkownikowi na dostęp do odpowiednich zasobów w aplikacji.'
							],
              value: null
						},
						{
							label: 'Opis',
							name: 'description',
							symbol: 'group-description',
							type: FormFieldType.Text,
              value: null
						}
					]
				}
			]
		));

    this.steps = steps;
  }

  private createStep(
    label: string, description: string, symbol: string, segments: SegmentModel[], completed: boolean = false): StepModel {

    const menu: StepModel = { label, description, symbol, segments, completed };
    return menu;
  }
}
