import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ViewChildren, QueryList, HostListener, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { TdLoadingService } from '@covalent/core/loading';
import { IPageChangeEvent } from '@covalent/core/paging';
import { TranslateService } from '@ngx-translate/core';

import { isNullOrUndefined } from 'util';

import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/scrolling';

import { AppConfig } from 'src/app/app.config';

import { GroupsUsersService } from './groups-users.service';

import { FilterObjectModel } from 'src/app/shared/models/filter.model';
import { SortObjectModel } from 'src/app/shared/models/sort.model';
import { PagingSettingsModel, PagingResultsModel } from 'src/app/shared/models/paging.model';
import { TableSettingsModel } from 'src/app/shared/models/table.model';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { UserModel } from 'src/app/shared/models/user.model';
import { GroupModel } from 'src/app/shared/models/group.model';

import { TableType } from 'src/app/shared/enums/table-type.enum';
import { FilterGroupType } from 'src/app/shared/enums/filters/filter-group-type.enum';
import { FilterGroupModel } from 'src/app/shared/models/filters/filter-group.model';

import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-groups-users',
  templateUrl: './groups-users.component.html',
  styleUrls: ['./groups-users.component.scss']
})
export class GroupsUsersComponent implements OnInit, AfterViewInit {

  config: any = {
    type: 'groups-users',
    header: {
      title: 'Grupy użytkowników',
      meta: 'Zarządzanie grupami użytkowników',
      icon: {
        name: 'users',
        color: ''
      }
    }
  };

  userLogged: UserModel;

  data: GroupModel[];
  currentItem: GroupModel;

  pagingResults: PagingResultsModel = new PagingResultsModel();

  filterGroupType: typeof FilterGroupType = FilterGroupType;

  constructor(
    public dialog: MatDialog,
    private scrollDispatcher: ScrollDispatcher,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private groupsUsersService: GroupsUsersService,
    public accountService: AccountService,
    public sidebarService: SidebarService) {

    this.data = [];
  }

  private initSemanticConfig() { }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);
    this.accountService.getUserLogged();

    this.sidebarService.headerConfig.next(this.config.header);
    this.sidebarService.headerVisible = false;
    this.sidebarService.subheaderVisible = false;
    this.sidebarService.menuVisible = false;
  }

  onRefreshData(params): void {
    const { pagingSettings, filters, sorts } = params;

    const filtersGroup: FilterGroupModel = new FilterGroupModel();
  }

  onAcceptFilter(event: any) {
  }

  onClearFilter(event: any) {
  }

  onRemoveFilter(event: any) {
    switch (event.type) {
      default:
        break;
    }
  }
}
