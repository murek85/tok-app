import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { GroupsUsersService } from './groups-users.service';

import { GroupsUsersComponent } from './groups-users.component';
import { GroupsUsersRoutingModule } from './groups-users-routing.module';

import { GroupUserDetailsComponent } from './details/group-user-details.component';
import { GroupUserFormComponent } from './form/group-user-form.component';

import { GroupsUsersFiltersComponent } from './filters/groups-users-filters.component';

import { GroupUserListComponent } from './list/user-list.component';
import { GroupUserTableComponent } from './table/user-table.component';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    GroupsUsersComponent,
    GroupsUsersFiltersComponent,

    GroupUserDetailsComponent,
    GroupUserFormComponent,

    GroupUserListComponent,
    GroupUserTableComponent
  ],
  imports: [
    GroupsUsersRoutingModule,

    APP_MODULES
  ],
  entryComponents: [],
  providers: [
    GroupsUsersService
  ]
})
export class GroupsUsersModule { }
