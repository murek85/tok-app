import { Component, OnInit, AfterViewInit, Input } from '@angular/core';

import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { UserModel } from 'src/app/shared/models/user.model';

declare var $: any;

@Component({
  selector: 'app-group-user-table',
  templateUrl: './user-table.component.html'
})
export class GroupUserTableComponent implements OnInit, AfterViewInit {

  @Input() data: any[];
  @Input() currentItem: any;

  constructor(
    private loadingService: TdLoadingService,
    private translateService: TranslateService) {
  }

  public ngOnInit() { }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  private initSemanticConfig() {
  }

  onSelected(item: any): void {
    this.currentItem = item;
  }

  // zwraca wartość zaznaczenia elementu
  isSelected(item: any): boolean {
    if (!this.currentItem) {
      return false;
    }
    return this.currentItem === item ? true : false;
  }
}
