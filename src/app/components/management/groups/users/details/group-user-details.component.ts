import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ViewChild, ElementRef, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { finalize } from 'rxjs/operators';

import { AppConfig } from 'src/app/app.config';

import { GroupsUsersService } from '../groups-users.service';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { GroupModel } from 'src/app/shared/models/group.model';
import { UserModel } from 'src/app/shared/models/user.model';

import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-group-user-details',
  templateUrl: './group-user-details.component.html',
  styleUrls: ['./group-user-details.component.scss']
})
export class GroupUserDetailsComponent implements OnInit, AfterViewInit {

  userLogged: UserModel;

  group: GroupModel;

  groupId = null;

  headerConfig = {
    title: 'Grupy użytkowników',
    meta: 'Szczegóły grupy użytkowników',
    icon: {
      name: 'users cog',
      color: ''
    }
  };

  error: any;

  selectedIndex = 0;

  menus = [
      { label: 'Szczegóły grupy', routerLink: '/management/groups/users/' }
  ];

  activeMenu = null;

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private accountService: AccountService,
    private groupsUsersService: GroupsUsersService,
    private sidebarService: SidebarService) {

    this.groupId = this.route.snapshot.params['id'];

    this.menus.forEach(menu => menu.routerLink += `${this.groupId}`); this.activeMenu = this.menus[0];
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);
    this.accountService.getUserLogged();

    this.sidebarService.menuBackPage.next({
      show: true,
      routerLink: `/management/groups/users/list`,
    });

    this.sidebarService.headerConfig.next(this.headerConfig);
    this.sidebarService.headerVisible = true;
    this.sidebarService.menuVisible = false;
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public changeTab($event) {
    const tab = $event as MatTabChangeEvent;
    switch (tab.index) {
    }
  }

  private initSemanticConfig() {
    $('.tabs-group .item').tab();
  }

  public saveForm() {
  }
}
