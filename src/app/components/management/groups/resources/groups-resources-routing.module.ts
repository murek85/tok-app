import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GroupsResourcesComponent } from './groups-resources.component';
import { GroupResourceDetailsComponent } from './details/group-resource-details.component';
import { GroupResourceFormComponent } from './form/group-resource-form.component';

import { AuthGuard } from 'src/app/core/services/auth.guard';
import { PermissionsTypes } from 'src/app/shared/enums/permissions-types.enum';

const onlyPermissions = [
  PermissionsTypes.GROUPS
];

const routes: Routes = [
  {
    path: 'management',
    children: [
      {
        path: 'groups/resources',
        children: [
          {
            path: '',
            redirectTo: 'list',
            pathMatch: 'full'
          },
          {
            path: 'list',
            component: GroupsResourcesComponent,
            data: {
              state: 'groups',
              permissions: { only: onlyPermissions, redirectTo: '/accessdenied' }
            },
            canActivate: [AuthGuard]
          },
          {
            path: 'form',
            children: [
              {
                path: '',
                component: GroupResourceFormComponent,
                canActivate: [AuthGuard]
              },
              {
                path: ':id',
                component: GroupResourceFormComponent,
                canActivate: [AuthGuard]
              }
            ]
          },
          {
            path: ':id',
            component: GroupResourceDetailsComponent,
            canActivate: [AuthGuard]
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class GroupsResourcesRoutingModule { }
