import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { GroupsResourcesService } from './groups-resources.service';

import { GroupsResourcesComponent } from './groups-resources.component';
import { GroupsResourcesRoutingModule } from './groups-resources-routing.module';

import { GroupResourceDetailsComponent } from './details/group-resource-details.component';
import { GroupResourceFormComponent } from './form/group-resource-form.component';

import { GroupsResourcesFiltersComponent } from './filters/groups-resources-filters.component';

import { GroupResourceListComponent } from './list/resource-list.component';
import { GroupResourceTableComponent } from './table/resource-table.component';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    GroupsResourcesComponent,
    GroupsResourcesFiltersComponent,

    GroupResourceDetailsComponent,
    GroupResourceFormComponent,

    GroupResourceListComponent,
    GroupResourceTableComponent
  ],
  imports: [
    GroupsResourcesRoutingModule,

    APP_MODULES
  ],
  entryComponents: [],
  providers: [
    GroupsResourcesService
  ]
})
export class GroupsResourcesModule { }
