import { Component, OnInit, Input, Output, AfterViewInit } from '@angular/core';

import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { FilterObjectModel } from 'src/app/shared/models/filter.model';
import { FilterGroupType } from 'src/app/shared/enums/filters/filter-group-type.enum';
import { FilterGroupModel } from 'src/app/shared/models/filters/filter-group.model';

declare var $: any;

@Component({
  selector: 'app-groups-resources-filters',
  styleUrls: ['./groups-resources-filters.component.scss'],
  templateUrl: './groups-resources-filters.component.html'
})
export class GroupsResourcesFiltersComponent implements OnInit, AfterViewInit {

  @Input() @Output() filters: FilterObjectModel<FilterGroupType>[];

  filterGroupType: typeof FilterGroupType = FilterGroupType;

  constructor(
    private loadingService: TdLoadingService,
    private translateService: TranslateService) {

    this.filters = [];
  }

  public ngOnInit() { }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  private initFilters() {
    this.filters.forEach(filter => {
      switch (filter.type) {

      }
    });
  }

  private initSemanticConfig() {
  }

  private findIndexWithFilters(value, filterType): number {
    const idx = this.filters.findIndex(filter => filter.value === (value) && filter.type === filterType);
    return idx;
  }

  private findIndexWithoutValue(filterType): number {
    const idx = this.filters.findIndex(filter => filter.type === filterType);
    return idx;
  }
}
