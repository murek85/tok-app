import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { ReportsComponent } from './reports.component';
import { ReportsRoutingModule } from './reports-routing.module';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    ReportsComponent
  ],
  imports: [
    ReportsRoutingModule,

    APP_MODULES
  ],
  providers: []
})
export class ReportsModule { }
