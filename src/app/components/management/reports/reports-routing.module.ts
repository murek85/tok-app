import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportsComponent } from './reports.component';

import { AuthGuard } from 'src/app/core/services/auth.guard';
import { PermissionsTypes } from 'src/app/shared/enums/permissions-types.enum';

const onlyPermissions = [
  PermissionsTypes.REPORTS
];

const routes: Routes = [
  {
    path: 'management',
    children: [
      {
        path: 'reports',
        component: ReportsComponent,
        data: {
          state: 'reports',
          permissions: { only: onlyPermissions, redirectTo: '/accessdenied' }
        },
        canActivate: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
