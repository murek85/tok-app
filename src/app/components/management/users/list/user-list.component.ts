import { Component, OnInit, AfterViewInit, Input } from '@angular/core';

import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { UserModel } from 'src/app/shared/models/user.model';

declare var $: any;

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html'
})
export class UserListComponent implements OnInit, AfterViewInit {

  @Input() data: UserModel[];
  @Input() currentItem: UserModel;

  constructor(
    private loadingService: TdLoadingService,
    private translateService: TranslateService) {
  }

  public ngOnInit() { }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  private initSemanticConfig() {
  }

  onSelected(item: UserModel): void {
    this.currentItem = item;
  }

  // zwraca wartość zaznaczenia elementu
  isSelected(item: UserModel): boolean {
    if (!this.currentItem) {
      return false;
    }
    return this.currentItem === item ? true : false;
  }
}
