import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from './users.component';
import { UserDetailsComponent } from './details/user-details.component';
import { UserFormComponent } from './form/user-form.component';

import { AuthGuard } from 'src/app/core/services/auth.guard';
import { PermissionsTypes } from 'src/app/shared/enums/permissions-types.enum';

const routes: Routes = [
  {
    path: 'management',
    children: [
      {
        path: 'users',
        children: [
          {
            path: '',
            redirectTo: 'list',
            pathMatch: 'full'
          },
          {
            path: 'list',
            component: UsersComponent,
            data: {
              state: 'users',
              permissions: { only: [PermissionsTypes.USERS], redirectTo: '/accessdenied' }
            },
            canActivate: [AuthGuard]
          },
          {
            path: 'form',
            children: [
              {
                path: '',
                component: UserFormComponent,
                data: {
                  state: 'users',
                  permissions: { only: [PermissionsTypes.USER_CREATE], redirectTo: '/accessdenied' }
                },
                canActivate: [AuthGuard]
              },
              {
                path: ':id',
                component: UserFormComponent,
                data: {
                  state: 'users',
                  permissions: { only: [PermissionsTypes.USER_EDIT], redirectTo: '/accessdenied' }
                },
                canActivate: [AuthGuard]
              }
            ]
          },
          {
            path: ':id',
            component: UserDetailsComponent,
            data: {
              state: 'users',
              permissions: { only: [PermissionsTypes.USER_EDIT], redirectTo: '/accessdenied' }
            },
            canActivate: [AuthGuard]
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class UsersRoutingModule { }
