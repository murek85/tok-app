import { Component, OnInit, Input, Output, AfterViewInit, SimpleChanges, SimpleChange, OnChanges } from '@angular/core';

import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { FilterObjectModel } from 'src/app/shared/models/filter.model';
import { FilterUserType } from 'src/app/shared/enums/filters/filter-user-type.enum';
import { FilterUserModel } from 'src/app/shared/models/filters/filter-user.model';

declare var $: any;

@Component({
  selector: 'app-users-filters',
  styleUrls: ['./users-filters.component.scss'],
  templateUrl: './users-filters.component.html'
})
export class UsersFiltersComponent implements OnInit, OnChanges, AfterViewInit {

  @Input() @Output() filters: FilterObjectModel<FilterUserType>[];

  filterUserType: typeof FilterUserType = FilterUserType;

  constructor(
    private loadingService: TdLoadingService,
    private translateService: TranslateService) {

    this.filters = [];
  }

  public ngOnInit() { }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }
  
  public ngOnChanges(changes: SimpleChanges) {
    const initialFilters: SimpleChange = changes.filters;
    if (initialFilters) {
      this.initFilters();
      // this.filters = initialFilters.currentValue;
    }
  }

  private initFilters() {
    this.filters.forEach(filter => {
      switch (filter.type) {
        case FilterUserType.OnlyUserEmailConfirmed: {
          $('.checkbox-only-user-email-confirmed').checkbox('check');
          break;
        }
        case FilterUserType.OnlyUserActive: {
          $('.checkbox-only-user-active').checkbox('check');
          break;
        }
        case FilterUserType.OnlyUserTemporaryAccess: {
          $('.checkbox-only-user-temporary-access').checkbox('check');
          break;
        }
      }
    });
  }

  private initSemanticConfig() {
    $('.checkbox-only-user-email-confirmed').checkbox({
      onChecked: () => {
        const index = this.findIndexWithoutValue(this.filterUserType.OnlyUserEmailConfirmed);
        if (index < 0) {
          this.filters.push({
            name: 'Ograniczenia',
            desc: 'Konta potwierdzone weryfikacją e-mail',
            type: this.filterUserType.OnlyUserEmailConfirmed,
            value: true
          });
        }
      },
      onUnchecked: () => {
        const index = this.findIndexWithoutValue(this.filterUserType.OnlyUserEmailConfirmed);
        if (index > -1) {
          this.filters.splice(index, 1);
        }
      }
    });

    $('.checkbox-only-user-active').checkbox({
      onChecked: () => {
        const index = this.findIndexWithoutValue(this.filterUserType.OnlyUserActive);
        if (index < 0) {
          this.filters.push({
            name: 'Ograniczenia',
            desc: 'Konta zablokowane',
            type: this.filterUserType.OnlyUserActive,
            value: true
          });
        }
      },
      onUnchecked: () => {
        const index = this.findIndexWithoutValue(this.filterUserType.OnlyUserActive);
        if (index > -1) {
          this.filters.splice(index, 1);
        }
      }
    });

    $('.checkbox-only-user-temporary-access').checkbox({
      onChecked: () => {
        const index = this.findIndexWithoutValue(this.filterUserType.OnlyUserTemporaryAccess);
        if (index < 0) {
          this.filters.push({
            name: 'Ograniczenia',
            desc: 'Konta z ograniczonym dostępem czasowym',
            type: this.filterUserType.OnlyUserTemporaryAccess,
            value: true
          });
        }
      },
      onUnchecked: () => {
        const index = this.findIndexWithoutValue(this.filterUserType.OnlyUserTemporaryAccess);
        if (index > -1) {
          this.filters.splice(index, 1);
        }
      }
    });
  }

  private findIndexWithFilters(value, filterType): number {
    const idx = this.filters.findIndex(filter => filter.value === (value) && filter.type === filterType);
    return idx;
  }

  private findIndexWithoutValue(filterType): number {
    const idx = this.filters.findIndex(filter => filter.type === filterType);
    return idx;
  }
}
