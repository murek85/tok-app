import { Component, Input, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CdkStepper } from '@angular/cdk/stepper';
import { TranslateService } from '@ngx-translate/core';
import { TdLoadingService } from '@covalent/core/loading';
import { iif } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { AccountService } from 'src/app/core/services/account.service';
import { UsersService } from '../users.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { UserModel } from 'src/app/shared/models/user.model';
import { ApplicationModel } from 'src/app/shared/models/application.model';
import { RoleModel } from 'src/app/shared/models/role.model';
import { ClaimModel } from 'src/app/shared/models/claim.model';
import { StepModel, SegmentModel, FormFieldModel } from 'src/app/shared/models/step.model';

import { FormFieldType } from 'src/app/shared/enums/form-field-type.enum';

import { AppConfig } from 'src/app/app.config';

import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-user-form',
  styleUrls: ['./user-form.component.scss'],
  templateUrl: './user-form.component.html'
})
export class UserFormComponent implements OnInit, AfterViewInit {

  roles: RoleModel[];
  applications: ApplicationModel[]
  claims: ClaimModel[];
  steps: StepModel[];

  headerConfig = {
    title: 'Użytkownicy',
    meta: 'Nowe konto użytkownika',
    symbol: 'user',
    icon: {
      name: 'user plus',
      color: ''
    }
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private translateService: TranslateService,
    private tdLoadingService: TdLoadingService,
    private accountService: AccountService,
    private usersService: UsersService,
    private sidebarService: SidebarService) {

    this.applications = [];
    this.roles = [];
    this.claims = [];
    this.steps = [];
  }

  public ngOnInit() {
    this.initApplications();
    this.initRoles();
    this.initClaims();
    this.initSteps();
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
    this.initSemanticForm();
  }

  public onChangeStep($event) {
    const stepper = $event as CdkStepper;
    switch (this.steps[stepper.selectedIndex].symbol) {
      case 'user-type': {
        this.initForm();
        break;
      }
      case 'user-data': {
        this.initForm();
        break;
      }
      case 'user-password': {
        this.initForm();
        break;
      }
      case 'user-claim': {
        this.initForm();
        break;
      }
      case 'user-restriction': {
        this.initForm();
        break;
      }
    }
  }

  public onPreviousStep($event) {
  }

  public onNextStep($event) {
  }

  public onSaveForm(model: UserModel) {
    $('body').toast({
      title: 'Pomyślnie zapisano',
      message: `Poprawnie utworzono nowe konto użytkownika o nazwie '${model.userName}'.`,
      showProgress: 'bottom',
      progressUp: true,
      onVisible: () => {
        this.router.navigate(['/management/users/list']); console.log(model);
      }
    });
  }

  public onBackForm($event) {
    this.router.navigate(['/management/users/list']);
  }

  private initSemanticForm() {
    this.initForm();
  }

  private initForm() {
    setTimeout(_ => {
      $(`.ui.form-${this.headerConfig.symbol}`).form({
        fields: {
          userName: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          },
          email: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          },
          firstName: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          },
          lastName: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          },
          password: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          },
          repeatPassword: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          },
          role: {
            rules: [
              {
                type: 'empty',
                prompt: `Pole '{name}' nie może być puste.`
              }
            ]
          }
        },
        inline : false
      });
    }, 0);
  }

  private initSemanticConfig() {
  }

  private initApplications() {
    const applications: ApplicationModel[] = [];
    applications.push({ name: 'epr', value: 'EPR' });

    this.applications = applications;
  }

  private initRoles() {
    const roles: RoleModel[] = [];
    roles.push({
      name: 'Administrator',
      value: 'ADMINISTRATOR',
      // application: { name: 'epr', value: 'EPR' }, claims: []
    });
    roles.push({
      name: 'Użytkownik zewnętrzny',
      value: 'UZYTKOWNIK',
      // application: { name: 'epr', value: 'EPR' }, claims: []
    });

    this.roles = roles;
  }

  private initClaims() {
    const claims: ClaimModel[] = [];
    claims.push({
      name: 'Zarządzanie użytkownikami',
      value: 'ZARZADZANIE_UZYTKOWNIKAMI',
      // application: { name: 'epr', value: 'EPR' }
    });
    claims.push({
      name: 'Zarządzanie uprawnieniami',
      value: 'ZARZADZANIE_UPRAWNIENIAMI',
      // application: { name: 'epr', value: 'EPR' }
    });
    claims.push({
      name: 'Raporty statystyczne',
      value: 'RAPORTY_STATYSTYCZNE',
      // application: { name: 'epr', value: 'EPR' }
    });

    this.claims = claims;
  }

  private initSteps() {
    const steps: StepModel[] = [];

    steps.push(this.createStep(
      'Typ konta',
      'Wybranie aplikacji w ramach której utworzona zostanie nowa rola.',
      'user-type',
      [
        {
          header: 'Typ konta',
          description: 'Podstawowe informacje o tworzonym koncie lub dane pobrane z usługi Active Directory.',
          fields: [
            {
              label: 'Typ konta',
              name: 'accountType',
              symbol: 'user-account-type',
              type: FormFieldType.Checkbox2,
              items: [
                {
                  label: 'Konto lokalne',
                  name: 'accountType',
                  symbol: 'account-type-local',
                  value: 'local'
                },
                {
                  label: 'Konto domenowe',
                  name: 'accountType',
                  symbol: 'account-type-domain',
                  value: 'domain'
                }
              ],
              value: 'local',
              hints: [
                'Wybranie typu konta na podstawie, którego użytkownik będzie logował się w aplikacji. Kreator pozwala na stworzenie konta ręcznie podając wszystkie wymagane pola lub na podstawie danych konta istniejącego w usłudze Ldap.'
              ]
            }
          ]
        }
      ]
    ));

    steps.push(this.createStep(
      'Konto użytkownika',
      'Wypełnienie podstawowych informacji o koncie użytkownika umożliwiających jego identyfikację.',
      'user-data',
      [
        {
          header: 'Konto domenowe',
          description: 'Uzupełnienie podstawowych informacji o koncie użytkownika.',
          fields: [
            {
              label: 'Konto z Ldap',
              name: 'ldap',
              symbol: 'user-ldap',
              type: FormFieldType.Single && FormFieldType.Search,
              api: {
                url:
                `${AppConfig.settings.system.apiUrl}/api/ldapusers/filter?query={query}`,
                fields: {
                  remoteValues: 'ldapUsers',
                  name: 'name',
                  value: 'objectGuid'
                },
                minCharacters: 0,
                onResponse: (response) => {
                  response.ldapUsers.map(user => user.name = `[${user.name}] ${user.displayName}`);
                  return response;
                }
              },
              value: null
            }
          ]
        },
        {
          header: 'Dane konta',
          description: 'Uzupełnienie podstawowych informacji o koncie użytkownika.',
          fields: [
            {
							label: 'Nazwa konta',
							name: 'userName',
							symbol: 'user-name',
              type: FormFieldType.Text,
              value: null,
              hints: [
                'Nazwa wymaga wprowadzenia znaków specjalnych.',
                'Nazwa wymaga minimum 8 znaków.'
              ]
						},
            {
							label: 'Adres e-mail',
							name: 'email',
							symbol: 'user-email',
              type: FormFieldType.Email,
              value: null,
              hints: [
                'Adres powinien zawierać odpowiednie znaki.'
              ]
						},
            {
							label: 'Imię (imiona)',
							name: 'firstName',
							symbol: 'user-firstname',
              type: FormFieldType.Text,
              value: null
						},
            {
							label: 'Nazwisko',
							name: 'lastName',
							symbol: 'user-lastname',
              type: FormFieldType.Text,
              value: null
						},
            {
							label: 'Numer kontaktowy',
							name: 'phoneNumber',
							symbol: 'user-phonenumber',
              type: FormFieldType.PhoneNumber,
              value: null
						}
          ]
        }
      ]
    ));

    steps.push(this.createStep(
      'Hasło użytkownika',
      'Wygenerowanie domyślnego hasła dostępu do tworzonego konta użytkownika.',
      'user-password',
      [
        {
          header: 'Hasło dostępu',
          description: 'Ustawienie hasła dostępu do konta użytkownika.',
          fields: [
            {
							label: 'Nowe hasło',
							name: 'password',
							symbol: 'user-password',
              type: FormFieldType.Password,
              value: null,
              hints: [
                'Hasło wymaga wprowadzenia znaków specjalnych.',
                'Hasło wymaga minimum 10 znaków.'
              ]
						},
            {
							label: 'Powtórz hasło',
							name: 'repeatPassword',
							symbol: 'user-repeat-password',
              type: FormFieldType.Password,
              value: null
						}
          ]
        }
      ]
    ));

    steps.push(this.createStep(
      'Role i uprawnienia',
      'Wybranie roli i opcjonalnych uprawnień umożliwiających wykonywanie odpowiednich akcji.',
      'user-claim',
      [
        {
          header: 'Role',
          description: 'Przypisanie odpowiedniej roli w aplikacji dla użytkownika.',
          fields: [
            {
              label: 'Role',
              name: 'role',
              symbol: 'user-role',
              type: FormFieldType.Multi,
              items: this.roles,
              value: null
            }
          ]
        },
        {
          header: 'Uprawnienia (opcjonalnie)',
          description: 'Opcjonalny dostęp do dodatkowych uprawnień w systemie.',
          fields: [
            {
              label: 'Uprawnienia',
              name: 'claim',
              symbol: 'user-claim',
              type: FormFieldType.Multi,
              items: this.claims,
              value: null
            }
          ]
        }
      ]
    ));

    steps.push(this.createStep(
      'Ograniczenia konta',
      'Ustawienie dodatkowych opcjonalnych ograniczeń na konto użytkownika w aplikacji.',
      'user-restriction',
      [
        {
          header: 'Ograniczenia konta',
          description: 'Pozostałe ustawienia lub inne ograniczenia konta użytkownika.',
          fields: [
            {
              label: 'Zweryfikowany adres e-mail',
              name: 'accountEmailConfirmed',
              symbol: 'user-account-email-confirmed',
              type: FormFieldType.Checkbox,
              value: null
            },
            {
              label: 'Konto zablokowane',
              name: 'accountBlocked',
              symbol: 'user-account-blocked',
              type: FormFieldType.Checkbox,
              value: null
            },
            {
              label: 'Konto aktywne',
              name: 'accountActive',
              symbol: 'user-account-active',
              type: FormFieldType.Checkbox,
              value: null
            },
            {
              label: 'Dostęp czasowy',
              name: 'accountAccess',
              symbol: 'user-account-access',
              type: FormFieldType.Checkbox,
              value: null
            }
          ]
        }
      ]
    ));

    this.steps = steps;
  }

  private createStep(
    label: string, description: string, symbol: string, segments: SegmentModel[], completed: boolean = false): StepModel {

    const menu: StepModel = { label, description, symbol, segments, completed };
    return menu;
  }
}
