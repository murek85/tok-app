import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ViewChildren, QueryList, HostListener, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { TdLoadingService } from '@covalent/core/loading';
import { IPageChangeEvent } from '@covalent/core/paging';
import { TranslateService } from '@ngx-translate/core';

import { isNullOrUndefined } from 'util';

import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/scrolling';

import { AppConfig } from 'src/app/app.config';

import { UsersService } from './users.service';

import { FilterObjectModel } from 'src/app/shared/models/filter.model';
import { SortObjectModel } from 'src/app/shared/models/sort.model';
import { PagingSettingsModel, PagingResultsModel } from 'src/app/shared/models/paging.model';
import { TableSettingsModel } from 'src/app/shared/models/table.model';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { UserModel } from 'src/app/shared/models/user.model';

import { TableType } from 'src/app/shared/enums/table-type.enum';
import { FilterUserType } from 'src/app/shared/enums/filters/filter-user-type.enum';
import { FilterUserModel } from 'src/app/shared/models/filters/filter-user.model';

import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, AfterViewInit {

  config: any = {
    type: 'users',
    header: {
      title: 'Użytkownicy',
      meta: 'Zarządzanie użytkownikami',
      icon: {
        name: 'users cog',
        color: ''
      }
    }
  };

  userLogged: UserModel;

  data: UserModel[];
  currentItem: UserModel;

  pagingResults: PagingResultsModel = new PagingResultsModel();

  filterUserType: typeof FilterUserType = FilterUserType;

  constructor(
    public dialog: MatDialog,
    private scrollDispatcher: ScrollDispatcher,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private usersService: UsersService,
    public accountService: AccountService,
    public sidebarService: SidebarService) {

    this.data = [];
  }

  private initSemanticConfig() { }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);
    this.accountService.getUserLogged();

    this.sidebarService.headerConfig.next(this.config.header);
    this.sidebarService.headerVisible = false;
    this.sidebarService.subheaderVisible = false;
    this.sidebarService.menuVisible = false;

    this.usersService.pagingResults.subscribe(pagingResults => this.pagingResults = pagingResults);

    this.usersService.users.subscribe(users => {
      this.data = users; this.currentItem = users[0]; this.tdLoadingService.resolve('listSyntax');
    });
  }

  onRefreshData(params): void {
    const { pagingSettings, filters, sorts } = params;

    const filtersUser: FilterUserModel = new FilterUserModel();

    // filtr konta niepotwierdzone
    let filtersObj: FilterObjectModel<FilterUserType>[] =
      filters.filter(x => x.type === FilterUserType.OnlyUserEmailConfirmed).map(x => {
        return x.value;
    });

    if (filtersObj.length > 0) {
      filtersUser.onlyUserEmailConfirmed = Boolean(filtersObj[0]);
    }

    // filtr konta zablokowane
    filtersObj =
      filters.filter(x => x.type === FilterUserType.OnlyUserActive).map(x => {
        return x.value;
    });

    if (filtersObj.length > 0) {
      filtersUser.onlyUserActive = Boolean(filtersObj[0]);
    }

    // filtr konta z ograniczeniem czasowym
    filtersObj =
      filters.filter(x => x.type === FilterUserType.OnlyUserTemporaryAccess).map(x => {
        return x.value;
    });

    if (filtersObj.length > 0) {
      filtersUser.onlyUserTemporaryAccess = Boolean(filtersObj[0]);
    }

    this.usersService.getUsers(pagingSettings, filtersUser);
  }

  onAcceptFilter(event: any) {
  }

  onClearFilter(event: any) {
    $('.checkbox-only-user-active').checkbox('uncheck');
    $('.checkbox-only-user-email-confirmed').checkbox('uncheck');
    $('.checkbox-only-user-temporary-access').checkbox('uncheck');
  }

  onRemoveFilter(event: any) {
    switch (event.type) {
      case this.filterUserType.OnlyUserActive: {
        $('.checkbox-only-user-active').checkbox('set unchecked');
        break;
      }
      case this.filterUserType.OnlyUserEmailConfirmed: {
        $('.checkbox-only-user-email-confirmed').checkbox('set unchecked');
        break;
      }
      case this.filterUserType.OnlyUserTemporaryAccess: {
        $('.checkbox-only-user-temporary-access').checkbox('set unchecked');
        break;
      }
    }
  }
}
