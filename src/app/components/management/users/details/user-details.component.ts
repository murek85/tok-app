import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ViewChild, ElementRef, HostListener, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { finalize } from 'rxjs/operators';

import { AppConfig } from 'src/app/app.config';

import { UsersService } from '../users.service';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { UserModel } from 'src/app/shared/models/user.model';
import { RoleModel } from 'src/app/shared/models/role.model';
import { ClaimModel } from 'src/app/shared/models/claim.model';

import * as moment from 'moment';

declare var $: any;

const calendarFormatter = {
  date: (date, settings) => {
    if (!date) {
        return '';
    }

    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    return day + '/' + month + '/' + year;
  }
};
const calendarText = {
  days: ['Nd', 'Pn', 'Wt', 'Śr', 'Czw', 'Pi', 'So'],
  months: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
  monthsShort: ['Sty', 'Lut', 'Mar', 'Kwi', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Paź', 'Lis', 'Gru'],
  today: 'dzisiaj',
  now: 'teraz',
  am: 'am',
  pm: 'pm'
};

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit, OnDestroy, AfterViewInit {

  error;

  aspId = null;

  user: UserModel;

  roles: RoleModel[];
  claims: ClaimModel[];
  causes;
  stats = [];

  headerConfig = {
    title: 'Użytkownicy',
    meta: 'Konto użytkownika',
    icon: {
      name: 'users cog',
      color: ''
    }
  };

  menus; activeMenu = null; selectedIndex = 0;

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private accountService: AccountService,
    private usersService: UsersService,
    private sidebarService: SidebarService) {

    this.aspId = this.route.snapshot.params['id'];

    this.menus = [];

    this.roles = [];
    this.claims = [];
    this.causes = [];
  }

  public ngOnInit() {
    this.sidebarService.menuBackPage.next({
      show: true,
      routerLink: `/management/users/list`,
    });

    this.usersService.user.subscribe(user => {
      this.user = user;

      this.stats = [
        { name: 'poprawne logowanie', icon: 'clock outline', type: 'datetime',
          value: `${moment(this.user?.lastValidLogin).format('DD-MM-YYYY HH:mm:ss') }`},
        { name: 'nieudane logowanie', icon: 'clock outline', type: 'datetime',
          value: `${moment(this.user?.lastIncorrectLogin).format('DD-MM-YYYY HH:mm:ss') }`}
      ];
    });
    this.usersService.getUser(this.aspId);

    this.initRoles();
    this.initClaims();
    this.initCauses();
    this.initMenus();
  }

  public ngOnDestroy() {
    $('.ui.modal.modal-account-set-password').remove();
    $('.ui.modal.modal-account-reset-password').remove();
    $('.ui.modal.modal-account-set-activate').remove();
    $('.ui.modal.modal-account-send-activate').remove();
    $('.ui.modal.modal-account-set-block').remove();
    $('.ui.modal.modal-account-delete').remove();
    $('.ui.modal.modal-account-send-message').remove();
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public changeTab($event) {
    const tab = $event as MatTabChangeEvent;
    console.log(tab);
    switch (tab.index) {
      case 0:
        break;
      case 1:
        this.initSemanticRoles();
        this.initSemanticClaims();
        break;
      case 2:
        break;
      case 3:
        this.initSemanticRegulations();
        break;
      case 4:
        this.initSemanticRestrictions();
        break;
    }
  }

  public onSaveForm() {
    this.tdLoadingService.register('listSyntax');
    $('body').toast({
      title: 'Pomyślnie zaktualizowano',
      message: `Poprawnie zaktualizowano dane konta użytkownika '${this.user.userName}'.`,
      showProgress: 'bottom',
      progressUp: true,
      onVisible: () => {
        this.tdLoadingService.resolve('listSyntax')
      }
    });
    // this.usersService.createUser(this.user)
    //   .pipe(finalize(() => this.tdLoadingService.resolve('listSyntax')))
    //   .subscribe(
    //     value => {
          
    //     },
    //     err => this.error = err.error,
    //     () => {}
    //   );
  }

  public onBackForm($event) {
    this.router.navigate(['/management/users/list']);
  }

  private initSemanticConfig() {
    $('.tabs-profile .item').tab();

    $('.account-settings').dropdown({
      action: 'hide'
    });

    $('.dropdown-causes-block').dropdown({
      clearable: true,
      context: 'td-layout',
      showOnFocus: false
    });

    $('.dropdown-causes-delete').dropdown({
      clearable: true,
      context: 'td-layout',
      showOnFocus: false
    });
  }

  private initSemanticRoles() {
    setTimeout(_ => {
      $('.dropdown-roles').dropdown({ });
    }, 0);
  }

  private initSemanticClaims() {
    setTimeout(_ => {
      $('.dropdown-claims').dropdown({ });
    }, 0);
  }

  private initSemanticRegulations() {
    setTimeout(_ => {
      $('.checkbox-account-1').checkbox({
        onChecked: () => {
        },
        onUnchecked: () => {
        }
      });

      $('.checkbox-account-2').checkbox({
        onChecked: () => {
        },
        onUnchecked: () => {
        }
      });

      $('.checkbox-account-3').checkbox({
        onChecked: () => {
        },
        onUnchecked: () => {
        }
      });
    }, 0);
  }

  private initSemanticRestrictions() {
    setTimeout(_ => {
      $('.checkbox-account-email-confirmed').checkbox({
        onChecked: () => {
        },
        onUnchecked: () => {
        }
      });

      $('.checkbox-account-blocked').checkbox({
        onChecked: () => {
        },
        onUnchecked: () => {
        }
      });

      $('.checkbox-account-active').checkbox({
        onChecked: () => {
        },
        onUnchecked: () => {
        }
      });

      $('.checkbox-account-access').checkbox({
        onChecked: () => {
        },
        onUnchecked: () => {
        }
      });

      // $('.calendar-access-lowerbound').calendar({
      //   formatter: calendarFormatter,
      //   text: calendarText,
      //   type: 'date',
      //   ampm: false,
      //   firstDayOfWeek: 1,
      //   endCalendar: $('#rangeend'),
      //   onChange: (value) => { }
      // });

      // $('.calendar-access-upperbound').calendar({
      //   formatter: calendarFormatter,
      //   text: calendarText,
      //   type: 'date',
      //   ampm: false,
      //   firstDayOfWeek: 1,
      //   startCalendar: $('#rangestart'),
      //   onChange: (value) => { }
      // });
    }, 0);
  }

  private initRoles() {
    const roles: RoleModel[] = [];
    roles.push({
      name: 'Administrator',
      value: 'ADMINISTRATOR',
      application: { name: 'epr', value: 'epr' }, claims: []
    });
    roles.push({ 
      name: 'Użytkownik zewnętrzny',
      value: 'UZYTKOWNIK',
      application: { name: 'epr', value: 'epr' }, claims: []
    });

    this.roles = roles;
  }

  private initClaims() {
    const claims: ClaimModel[] = [];
    claims.push({
      name: 'Zarządzanie kontami',
      value: 'ZARZADZANIE_KONTAMI',
      application: { name: 'epr', value: 'EPR' }
    });
    claims.push({
      name: 'Zarządzanie uprawnieniami',
      value: 'ZARZADZANIE_UPRAWNIENIAMI',
      application: { name: 'epr', value: 'EPR' }
    });
    claims.push({
      name: 'Raporty statystyczne',
      value: 'RAPORTY_STATYSTYCZNE',
      application: { name: 'epr', value: 'EPR' }
    });

    this.claims = claims;
  }

  private initCauses() {
    this.causes = [
      { name: 'Przyczyna pierwsza', value: 'CAUSE1' },
      { name: 'Przyczyna druga', value: 'CAUSE2' },
      { name: 'Przyczyna trzecia', value: 'CAUSE3' },
      { name: 'Przyczyna czwarta', value: 'CAUSE4' },
      { name: 'Przyczyna piąta', value: 'CAUSE5' }
    ];
  }

  private initMenus() {
    this.menus = [
      { label: 'Szczegóły konta', routerLink: '/management/users/' },
      { label: 'Bezpieczeństwo', routerLink: '/management/users/' },
      { label: 'Role i uprawnienia', routerLink: '/management/users/' },
      { label: 'Historia operacji', routerLink: '/management/users/' }
    ];

    this.menus.forEach(menu => menu.routerLink += `${this.aspId}`);
    this.activeMenu = this.menus[0];
  }

  ///

  public accountSetPassword() {
    $('.ui.modal.modal-account-set-password')
      .modal({
        context: 'td-layout',
        transition: 'fly left',
        closable: false,
        onDeny: () => {
        },
        onApprove: () => {
        }
      })
      .modal('show');
  }

  public accountResetPassword() {
    $('.ui.modal.modal-account-reset-password')
      .modal({
        context: 'td-layout',
        transition: 'fly left',
        closable: false,
        onDeny: () => {
        },
        onApprove: () => {
        }
      })
      .modal('show');
  }

  public accountSetActivate() {
    $('.ui.modal.modal-account-set-activate')
      .modal({
        context: 'td-layout',
        transition: 'fly left',
        closable: false,
        onDeny: () => {
        },
        onApprove: () => {
        }
      })
      .modal('show');
  }

  public accountSendActivate() {
    $('.ui.modal.modal-account-send-activate')
      .modal({
        context: 'td-layout',
        transition: 'fly left',
        closable: false,
        onDeny: () => {
        },
        onApprove: () => {
        }
      })
      .modal('show');
  }

  public accountSetBlock() {
    $('.ui.modal.modal-account-set-block')
      .modal({
        context: 'td-layout',
        transition: 'fly left',
        closable: false,
        onDeny: () => {
        },
        onApprove: () => {
        }
      })
      .modal('show');
  }

  public accountDelete() {
    $('.ui.modal.modal-account-delete')
      .modal({
        context: 'td-layout',
        transition: 'fly left',
        closable: false,
        onDeny: () => {
        },
        onApprove: () => {
        }
      })
      .modal('show');
  }

  public accountSendMessage() {
    $('.ui.modal.modal-account-send-message')
      .modal({
        context: 'td-layout',
        transition: 'fly left',
        closable: false,
        onDeny: () => {
        },
        onApprove: () => {
        }
      })
      .modal('show');
  }
}
