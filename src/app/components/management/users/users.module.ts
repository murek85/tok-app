import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { UsersService } from './users.service';

import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users-routing.module';

import { UserDetailsComponent } from './details/user-details.component';
import { UserFormComponent } from './form/user-form.component';

import { UsersFiltersComponent } from './filters/users-filters.component';

import { UserTableComponent } from './table/user-table.component';
import { UserListComponent } from './list/user-list.component';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    UsersComponent,
    UsersFiltersComponent,

    UserDetailsComponent,
    UserFormComponent,

    UserTableComponent,
    UserListComponent
  ],
  imports: [
    UsersRoutingModule,

    APP_MODULES
  ],
  entryComponents: [],
  providers: [
    UsersService
  ]
})
export class UsersModule { }
