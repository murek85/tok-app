import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { UsersHttpService } from 'src/app/core/services/http/users.service';

import { TdLoadingService } from '@covalent/core/loading';

import { PagingSettingsModel, PagingResultsModel } from 'src/app/shared/models/paging.model';
import { FilterUserModel } from 'src/app/shared/models/filters/filter-user.model';
import { UserModel } from 'src/app/shared/models/user.model';
import { ResultModel } from 'src/app/shared/models/result.model';
import { RoleModel } from 'src/app/shared/models/role.model';

@Injectable()
export class UsersService {

  private usersSource = new BehaviorSubject<UserModel[]>([]);
  private userSource = new BehaviorSubject<UserModel>(null);
  private filtersSource = new BehaviorSubject<FilterUserModel>(null);
  private pagingResultsSource = new BehaviorSubject<PagingResultsModel>(new PagingResultsModel());

  users = this.usersSource.asObservable();
  user = this.userSource.asObservable();
  filters = this.filtersSource.asObservable();
  pagingResults = this.pagingResultsSource.asObservable();

  changeUsers(data: UserModel[]) {
    this.usersSource.next(data);
  }

  changeUser(data: UserModel) {
      this.userSource.next(data);
  }

  changeFilters(data: FilterUserModel) {
      this.filtersSource.next(data);
  }

  changePagingResults(data: PagingResultsModel) {
      this.pagingResultsSource.next(data);
  }

  constructor(
    private tdLoadingService: TdLoadingService,
    private usersHttpService: UsersHttpService) { }

  public getUsers(pagingSettings: PagingSettingsModel, filters: FilterUserModel) {
    this.tdLoadingService.register('listSyntax');
    this.usersHttpService.getUsers(pagingSettings, filters).subscribe(
      (result: ResultModel<UserModel[]>) => {
        this.changeFilters(filters);
        this.changePagingResults(result.paging);

        result.data.forEach((user: UserModel) => {
          const roles: RoleModel[] = [];
          roles.push(
            {
              name: 'Administrator',
              value: 'CAL_ADMINISTRATOR',
              application: { name: 'epr', value: 'epr' },
              claims: []
            }
          );
          user.roles = roles;
        });

        this.changeUsers(result.data);
      },
      err => this.tdLoadingService.resolve('listSyntax'),
      () => this.tdLoadingService.resolve('listSyntax')
    );
  }

  public getUser(aspId) {
    this.tdLoadingService.register('listSyntax');
    this.usersHttpService.getUser(aspId).subscribe(
      user => this.changeUser(user),
      err => this.tdLoadingService.resolve('listSyntax'),
      () => this.tdLoadingService.resolve('listSyntax')
    );
  }

  public createUser(user: UserModel) {
    return this.usersHttpService.createUser(user);
  }

  public updateUser(user: UserModel) {
    return this.usersHttpService.updateUser(user);
  }

  public deleteUser(aspId) {
    return this.usersHttpService.deleteUser(aspId);
  }

  public loadUserLogged() {
    return this.usersHttpService.getUserLogged();
  }
}
