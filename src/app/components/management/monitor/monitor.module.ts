import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { MonitorComponent } from './monitor.component';
import { MonitorRoutingModule } from './monitor-routing.module';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    MonitorComponent
  ],
  imports: [
    MonitorRoutingModule,

    APP_MODULES
  ],
  providers: []
})
export class MonitorModule { }
