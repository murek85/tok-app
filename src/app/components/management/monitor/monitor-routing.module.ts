import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MonitorComponent } from './monitor.component';

import { AuthGuard } from 'src/app/core/services/auth.guard';
import { PermissionsTypes } from 'src/app/shared/enums/permissions-types.enum';

const onlyPermissions = [
  PermissionsTypes.MONITOR
];

const routes: Routes = [
  {
    path: 'management',
    children: [
      {
        path: 'monitor',
        component: MonitorComponent,
        data: {
          state: 'monitor',
          permissions: { only: onlyPermissions, redirectTo: '/accessdenied' }
        },
        canActivate: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class MonitorRoutingModule { }
