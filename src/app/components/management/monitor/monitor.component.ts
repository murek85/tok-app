import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef, ChangeDetectorRef, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { NgxWidgetGridComponent } from 'ngx-widget-grid';
import { isNullOrUndefined } from 'util';

import { filter } from 'rxjs/operators';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { DashboardConfig } from 'src/app/shared/models/dashboard-config.model';
import { DashboardWidget } from 'src/app/shared/models/dashboard-widget.model';
import { UserModel } from 'src/app/shared/models/user.model';

import { DashboardType } from 'src/app/shared/enums/dashboard-type.enum';
import { WidgetType } from 'src/app/shared/enums/widget-type.enum';

import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-monitor',
  templateUrl: './monitor.component.html',
  styleUrls: ['./monitor.component.scss']
})
export class MonitorComponent implements OnInit, AfterViewInit {

  userLogged: UserModel;

  system = { version: '1.0.0', date: '21-10-2019', type: 'dev', appName: 'Nazwa aplikacji' };
  languages: any[] = AppConfig.settings.i18n.availableLanguages;

  selectedIndex = 0;

  notifications = [];

  headerConfig = {
    title: 'Monitorowanie aplikacji',
    meta: '',
    icon: {
      name: '',
      color: ''
    }
  };

  config: DashboardConfig = {
    type: DashboardType.Monitor,
    title: 'Monitorowanie aplikacji'
  };
  widgets: Array<DashboardWidget> = [
    {
      position: { top: 1, left: 1, height: 39, width: 40 },
      settings: { type: WidgetType.Cpu, header: 'Procesor i pamięć', icon: 'server', theme: 'none' },
      active: true
    },
    {
      position: { top: 1, left: 81, height: 39, width: 40 },
      settings: { type: WidgetType.Disk, header: 'Dysk', icon: 'hdd', theme: 'none' },
      active: true
    },
    {
      position: { top: 1, left: 41, height: 39, width: 40 },
      settings: { type: WidgetType.Applications, header: 'Aplikacje', icon: 'tasks', theme: 'none' },
      active: true
    }
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private sidebarService: SidebarService,
    public accountService: AccountService) {
  }

  private initSemanticConfig() {
    $('.settings').dropdown({
      action: 'hide'
    });
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);

    this.sidebarService.headerConfig.next(this.headerConfig);
    this.sidebarService.headerVisible = true;

    this.system = {
      version: AppConfig.settings.system.version,
      date: AppConfig.settings.system.date,
      type: AppConfig.settings.system.type,
      appName: AppConfig.settings.system.appName
    };
  }
}
