import { NgxMAuthOidcConfig } from './core/services/oidc/oidc-config';
import { AppConfig } from './app.config';

export function authConfig(url: string): NgxMAuthOidcConfig {
	return {
		origin: AppConfig.settings.system.appUrl,
		issuer: url + '/',
		loginUrl: url + '/login',
		redirectUri: AppConfig.settings.system.appUrl + '/auth',
		responseType: AppConfig.settings.oidc.responseType,
		clientId: AppConfig.settings.oidc.clientId,
		dummyClientSecret: AppConfig.settings.oidc.clientSecret,
		scope: AppConfig.settings.oidc.scope,
		requireHttps: AppConfig.settings.oidc.requireHttps
	};
}
